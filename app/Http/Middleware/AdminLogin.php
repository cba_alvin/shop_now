<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class AdminLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $check = null)
    {
        if (!Auth::check()) {
            return redirect()->route('admin.getlogin')->withCookie(cookie()->forget('allowCkfinder'));
        } else {
            if (Auth::user()->status < 2) {
                return redirect()->route('admin.getlogin')->withCookie(cookie()->forget('allowCkfinder'));
            } else {
                return $next($request);
            }
        }
    }
}
