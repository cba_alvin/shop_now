<?php

namespace App\Http\Controllers\Auth\Admin;

use App\Http\Controllers\Controller;
use Auth;
use Cookie;
use Illuminate\Http\Request;
use Redirect;
use Session;
use Validator;

class AuthController extends Controller
{

    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    public function getLogin()
    {
        return view('admin.auth.login');
    }

    public function getLogout()
    {
        Auth::logout();
        Session::flush();
        return Redirect::to('/')->withCookie(cookie()->forget('allowCkfinder'));
    }

    public function postLogin(Request $request)
    {
        $rules = [
            'email'    => 'required|email',
            'password' => 'required|min:3',
        ];
        $messages = [
            'email.required'    => 'Email là trường bắt buộc',
            'email.email'       => 'Email không đúng định dạng',
            'password.required' => 'Mật khẩu là trường bắt buộc',
            'password.min'      => 'Mật khẩu phải chứa ít nhất 8 ký tự',
        ];
        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            $email    = $request->input('email');
            $password = $request->input('password');
            $remember = $request->input('remember') ? true : false;

            if (Auth::attempt(['email' => $email, 'password' => $password], $remember)) {
                return redirect()->intended(route('admin.dashboard'))->withCookie(cookie()->forever('allowCkfinder', "1"));
            } else {
                $status = json_encode(['status' => 'Error', 'message' => 'Sorry, the member name and password you entered do not match. Please try again.']);
                return redirect()->back()->withInput()->with('status', $status);
            }
        }
    }

    public function Profile()
    {
        $user = Auth::user();
        return view('admin.auth.login');

    }
}
