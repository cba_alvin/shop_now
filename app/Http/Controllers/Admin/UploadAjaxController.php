<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;

class UploadAjaxController extends Controller
{
    public function uploadImageProduct($type = null, Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'file' => 'image',
            ],
            [
                'file.image' => 'The file must be an image (jpeg, png, bmp, gif, or svg)',
            ]);
        if ($validator->fails()) {
            return json_encode(array(
                'status' => false,
                'errors' => $validator->errors(),
            ));
        }
        $extension = $request->file('file')->getClientOriginalExtension();
        if ($type) {
            $dir = 'uploads/' . $type . '/';
        } else {
            $dir = 'uploads/product/';
        }
        $filename = uniqid() . '_' . time() . '.' . $extension;
        $request->file('file')->move($dir, $filename);
        $nameAndPatch = $dir . $filename;
        return json_encode(['status' => true, 'filename' => $nameAndPatch]);
    }
}
