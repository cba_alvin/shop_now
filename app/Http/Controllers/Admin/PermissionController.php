<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;

class PermissionController extends Controller
{
    public function index()
    {
        $data = Permission::all();

        return view('admin.permission.list', compact('data'));
    }

    public function getStore($id = null)
    {
        $permission = Permission::find($id);
        if ($permission) {
            return view('admin.permission.store', compact('permission', 'id'));
        }
        return view('admin.permission.store');
    }

    public function postStore($id = null, Request $request)
    {

        if ($id) {
            $request->validate([
                'name' => 'required|alpha|max:255',
            ]);
            $permission = Permission::find($id);
            $status     = json_encode(['status' => 'Success', 'message' => 'Successfully updated permission']);
        } else {
            $request->validate([
                'name' => 'required|alpha|unique:permissions|max:255',
            ]);
            $permission = new Permission;
            $status     = json_encode(['status' => 'Success', 'message' => 'Successfully created new permission']);
        }
        $permission->name = $request->name;
        $permission->save();

        return redirect()->route('admin.permission.list')->with('status', $status);
    }

    public function postDelete(Request $request)
    {

        $permission = Permission::find($request->id);
        $status     = json_encode(['status' => 'Error', 'message' => 'Can not find this id : ' . $request->id]);
        if ($permission) {
            $permission->delete();
            $status = json_encode(['status' => 'Success', 'message' => 'Success delete permission']);
        }
        return redirect()->route('admin.permission.list')->with('status', $status);
    }
}
