<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CategoryProduct;
use Illuminate\Http\Request;

class CategoryProductController extends Controller
{
    public function index()
    {
        $data = CategoryProduct::select()->whereNotIn('id', [1])->get();
        return view('admin.product.category.list', compact('data'));
    }

    public function getStore($id = null)
    {
        $categoryList = CategoryProduct::select()->where('parent_id', 0)->get();
        if ($id) {
            $category = CategoryProduct::find($id);
            if ($category) {
                if ($category->children->count() > 0) {
                    $categoryList = null;
                }
                return view('admin.product.category.storenoparent', compact('category', 'id', 'categoryList'));
            }
            $status = json_encode(['status' => 'Error', 'message' => 'Can not find this id : ' . $id]);
            return redirect()->route('admin.products.category.list')->with('status', $status);
        }
        return view('admin.product.category.storenoparent', compact('categoryList'));
    }

    public function postStore($id = null, Request $request)
    {
        if ($id) {
            $request->validate([
                'name' => 'required|max:255',
            ]);
            $category = CategoryProduct::find($id);
            $status   = json_encode(['status' => 'Success', 'message' => 'Successfully updated category id : ' . $id]);
        } else {
            $request->validate([
                'name' => 'required|unique:category_products|max:255',
            ]);
            $category = new CategoryProduct;
            $status   = json_encode(['status' => 'Success', 'message' => 'Successfully created new category']);
        }
        $category->name  = $request->name;
        $category->image = $request->image;
        // if ($request->parent_id != 0) {
        //     $category->parent_id = $request->parent_id;
        // }

        $category->parent_id        = 1; // set mode no parent and set id 1 = all
        $category->pin_index_status = $request->pin_index_status ? 1 : 0;
        $category->status           = $request->status ? 1 : 0;
        $category->save();

        return redirect()->route('admin.products.category.list')->with('status', $status);
    }

    public function postDelete(Request $request)
    {
        if ($request->id != 1) {
            $category = CategoryProduct::find($request->id);
            if ($category) {
                $category->views()->delete();
                $category->delete();
                $status = json_encode(['status' => 'Success', 'message' => 'Successfully deleted category']);
                return redirect()->route('admin.products.category.list')->with('status', $status);
            }
            $status = json_encode(['status' => 'Error', 'message' => 'Can not find this id : ' . $id]);
            return redirect()->route('admin.products.category.list')->with('status', $status);
        }
        $status = json_encode(['status' => 'Error', 'message' => 'Can not find this id : ' . $request->id]);
        return redirect()->route('admin.products.category.list')->with('status', $status);
    }
}
