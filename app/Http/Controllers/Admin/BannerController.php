<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Banner;
use Illuminate\Http\Request;

class BannerController extends Controller
{
    public function index()
    {
        $data = Banner::all();
        return view('admin.banner.list', compact('data'));
    }

    public function getStore($id = null)
    {
        if ($id) {
            $banner = Banner::find($id);
            if ($banner) {
                return view('admin.banner.store', compact('banner', 'id'));
            }
            $status = json_encode(['status' => 'Error', 'message' => 'Can not find this id : ' . $id]);
            return redirect()->route('admin.banner.list')->with('status', $status);
        }
        return view('admin.banner.store');
    }

    public function postStore($id = null, Request $request)
    {
        $request->validate([
            'title'   => 'required|max:255',
            'content' => 'required|max:255',
            'url'     => 'required|max:255',
        ]);
        if ($id) {
            $banner = Banner::find($id);
            $status = json_encode(['status' => 'Success', 'message' => 'Successfully updated banner id : ' . $id]);
        } else {
            $banner = new Banner;
            $status = json_encode(['status' => 'Success', 'message' => 'Successfully created new banner']);
        }
        $banner->title   = $request->title;
        $banner->content = $request->content;
        $banner->url     = $request->url;
        $banner->image   = $request->image;
        $banner->save();
        return redirect()->route('admin.banner.list')->with('status', $status);

    }

    public function postDelete(Request $request)
    {
        $banner = Banner::find($request->id);
        if ($banner) {
            $status = json_encode(['status' => 'Success', 'message' => 'Successfully deleted banner ' . $banner->title]);
            $banner->delete();
            return redirect()->route('admin.banner.list')->with('status', $status);
        }
        $status = json_encode(['status' => 'Error', 'message' => 'Can not find this id : ' . $id]);
        return redirect()->route('admin.banner.list')->with('status', $status);
    }
}
