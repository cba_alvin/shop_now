<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
    public function index()
    {
        $data = Role::all();
        return view('admin.role.list', compact('data'));
    }

    public function getStore($id = null)
    {
        $role       = Role::find($id);
        $permission = Permission::all();
        if ($role) {
            return view('admin.role.store', compact('role', 'permission', 'id'));
        }
        return view('admin.role.store', compact('permission'));
    }

    public function postStore($id = null, Request $request)
    {
        $requestData = $request->except('permissions');
        $permissions = $request->permissions;
        if ($id) {
            $request->validate([
                'name' => 'required|alpha|max:255',
            ]);
            $role = Role::find($id);
            if ($role) {
                $role->update($requestData);
                $role->syncPermissions($permissions);

                $status = json_encode(['status' => 'Success', 'message' => 'Successfully updated role']);
            } else {
                $status = json_encode(['status' => 'Error', 'message' => 'Can not find this id ! ']);
            }
        } else {
            $request->validate([
                'name' => 'required|alpha|unique:roles|max:255',
            ]);

            $role = Role::create($requestData);
            $role->givePermissionTo($permissions);
            $status = json_encode(['status' => 'Success', 'message' => 'Successfully created new role']);
        }

        return redirect()->route('admin.role.list')->with('status', $status);
    }

    public function destroy($id)
    {
        Role::destroy($id);

        return redirect('admin/role')->with('flash_message', 'Role deleted!');
    }
}
