<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CategoryProduct;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{

    public function index()
    {
        $data = Product::all();
        return view('admin.product.product.list', compact('data', 'categoryList'));
    }

    public function getStore($id = null)
    {
        $categoryList = CategoryProduct::select()->where('parent_id', 0)->where('status', 1)->get();
        if ($id) {
            $product = Product::find($id);
            if ($product) {
                return view('admin.product.product.store', compact('product', 'id', 'categoryList'));
            }
            $status = json_encode(['status' => 'Error', 'message' => 'Can not find this id : ' . $id]);
            return redirect()->route('admin.products.product.list')->with('status', $status);
        }
        return view('admin.product.product.store', compact('categoryList'));
    }

    public function postStore($id = null, Request $request)
    {
        $request->validate([
            'name'                => 'required|max:255',
            'category_product_id' => 'required|max:255',
            'image'               => 'required',
        ]);
        $images = $this->changeKeyImagePost($request->image);
        if ($id) {
            $product = Product::find($id);
            if ($product) {
                $status = json_encode(['status' => 'Success', 'message' => 'Successfully updated product id : ' . $product->name]);
            } else {
                $status = json_encode(['status' => 'Error', 'message' => 'Can not find this id : ' . $id]);
            }
        } else {
            $product = new Product;
            $status  = json_encode(['status' => 'Success', 'message' => 'Successfully created new product']);
        }
        if ($images[1]['name']) {
            $product->image_index        = $images[1]['name'];
            $product->json_image_details = json_encode($images);
        }
        $product->json_details        = json_encode($request->detail);
        $product->name                = $request->name;
        $product->name_slug           = $this->toSlug($request->name);
        $product->category_product_id = $request->category_product_id;
        $product->price               = str_replace(',', '', $request->price);
        $product->price_sale          = str_replace(',', '', $request->price_sale);
        $product->discription         = $request->discription;
        $product->status              = $request->status ? 1 : 0;
        $product->save();

        return redirect()->route('admin.products.product.list')->with('status', $status);
    }

    public function postDelete(Request $request)
    {
        $product = Product::find($request->id);
        if ($product) {
            $product->views()->delete();
            $product->delete();

            $status = json_encode(['status' => 'Success', 'message' => 'Successfully deleted product']);
            return redirect()->route('admin.products.product.list')->with('status', $status);
        }
        $status = json_encode(['status' => 'Error', 'message' => 'Can not find this id : ' . $id]);
        return redirect()->route('admin.products.product.list')->with('status', $status);
    }

    public function changeKeyImagePost($data)
    {
        if ($data[1]['name'] != null) {
            $k = 1;
            foreach ($data as $image) {
                if ($image['name'] != '' || $image['name'] != null) {
                    $images[$k]['name'] = $image['name'];
                }

                $k++;
            }
            return $images;
        }
        return $images = null;
    }

    public function toSlug($str)
    {
        $str = trim(mb_strtolower($str));
        $str = preg_replace('/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/', 'a', $str);
        $str = preg_replace('/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/', 'e', $str);
        $str = preg_replace('/(ì|í|ị|ỉ|ĩ)/', 'i', $str);
        $str = preg_replace('/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/', 'o', $str);
        $str = preg_replace('/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/', 'u', $str);
        $str = preg_replace('/(ỳ|ý|ỵ|ỷ|ỹ)/', 'y', $str);
        $str = preg_replace('/(đ)/', 'd', $str);
        $str = preg_replace('/[^a-z0-9-\s]/', '', $str);
        $str = preg_replace('/([\s]+)/', '-', $str);
        return $str;
    }

    public function sellThrough($price, $price_sale)
    {
        return 100 - (($price_sale / $price_sale) * 100);
    }
}
