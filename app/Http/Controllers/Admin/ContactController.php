<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Contact;

class ContactController extends Controller
{
    public function index()
    {
        $data = Contact::select()->orderBy('views', 'ASC')->get();
        return view('admin.contact.list', compact('data'));
    }

    public function detail($id = null)
    {
        if ($id) {
            $contact        = Contact::find($id);
            $contact->views = $contact->views + 1;
            $contact->save();
            if ($contact) {
                return view('admin.contact.detail', compact('contact', 'id'));
            }
        }
        $status = json_encode(['status' => 'Error', 'message' => 'Can not find this id : ' . $id]);
        return redirect()->route('admin.contact.list')->with('status', $status);
    }
}
