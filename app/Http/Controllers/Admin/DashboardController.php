<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Banner;
use App\Models\CategoryProduct;
use App\Models\Contact;
use App\Models\Product;

class DashboardController extends Controller
{

    public function getIndex()
    {
        $product         = Product::select();
        $cateogryProduct = CategoryProduct::select()->where('parent_id', 1);
        $contact         = Contact::select();
        $banner          = Banner::select();
        return view('admin.dashboard', compact('product', 'cateogryProduct', 'contact', 'banner'));
    }
}
