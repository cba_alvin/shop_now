<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\About;
use Illuminate\Http\Request;

class AboutController extends Controller
{
    public function getStore()
    {
        $about = About::find(1);
        if ($about) {
            return view('admin.about.store', compact('about'));
        }
        return view('admin.about.store');
    }

    public function postStore(Request $request)
    {
        $request->validate([
            'title'       => 'required|max:255',
            'discription' => 'required',
        ]);
        $images = $this->changeKeyImagePost($request->image);
        $about  = About::find(1);
        if ($about) {
            $status = json_encode(['status' => 'Success', 'message' => 'Successfully update about']);
        } else {
            $about  = new About;
            $status = json_encode(['status' => 'Success', 'message' => 'Successfully update about']);
        }
        if ($images[1]['name']) {
            $about->json_image_details = json_encode($images);
        }
        $about->title       = $request->title;
        $about->discription = $request->discription;
        $about->save();

        return redirect()->route('admin.about.store')->with('status', $status);
    }
    public function changeKeyImagePost($data)
    {
        if ($data[1]['name'] != null) {
            $k = 1;
            foreach ($data as $image) {
                if ($image['name'] != '' || $image['name'] != null) {
                    $images[$k]['name'] = $image['name'];
                }

                $k++;
            }
            return $images;
        }
        return $images = null;
    }
}
