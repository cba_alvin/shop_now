<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    public function index()
    {
        $data = User::all();
        return view('admin.user.list', compact('data'));
    }

    public function getStore($id = null)
    {
        $roles = Role::select()->get();
        $user  = User::find($id);
        if ($user) {
            return view('admin.user.store', compact('user', 'roles', 'id'));
        }
        return view('admin.user.store', compact('roles'));
    }

    public function postStore($id = null, Request $request)
    {
        $requestData = $request->except('roles');
        $roles       = $request->roles;
        if ($id) {
            $request->validate([
                'name'    => 'required|max:255',
                'email'   => 'required|max:255',
                'address' => 'max:255',
                'phone'   => 'numeric',
            ]);
            $user = User::find($id);
            if ($user) {
                $user->update($requestData);
                $user->syncRoles($request->roles);
                $status = json_encode(['status' => 'Success', 'message' => 'Successfully updated user']);
            } else {
                $status = json_encode(['status' => 'Error', 'message' => 'Can not find this id ! ']);
            }
        } else {
            $request->validate([
                'name'    => 'required|max:255',
                'email'   => 'required|unique:users|max:255',
                'address' => 'max:255',
                'phone'   => 'numeric|min:8|max:13',
            ]);

            $user = User::create($requestData);
            $user->assignRole($roles);
            $status = json_encode(['status' => 'Success', 'message' => 'Successfully created new user']);
        }

        return redirect()->route('admin.user.list')->with('status', $status);
    }

    public function destroy($id)
    {
        User::destroy($id);

        return redirect('admin/role')->with('flash_message', 'Role deleted!');
    }
}
