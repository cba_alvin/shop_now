<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\About;
use App\Models\Contact;
use Illuminate\Http\Request;
use Intervention\Image\Image;

class OtherController extends Controller
{
    public function about()
    {
        $about = About::find(1);
        return view('user.other.about', compact('about'));
    }

    public function contact()
    {
        return view('user.other.contact');
    }

    public function postContact(Request $request)
    {
        $request->validate([
            'name'    => 'required|max:255',
            'phone'   => 'required|numeric',
            'email'   => 'required|email',
            'content' => 'required',
        ]);
        $contact          = new Contact;
        $contact->name    = $request->name;
        $contact->email   = $request->email;
        $contact->phone   = $request->phone;
        $contact->content = $request->content;

        if ($request->image) {
            foreach ($request->image as $key => $image) {
                if ($key < 6) {
                    $fileExtension = $image->getClientOriginalExtension();
                    $fileName      = time() . "_" . rand(0, 9999999) . "_" . md5(rand(0, 9999999)) . "." . $fileExtension;
                    $uploadPath    = public_path('/image_contacts');
                    $image->move($uploadPath, $fileName);
                    $images[$key]['patch'] = '/image_contacts/' . $fileName;
                }
            }
            $contact->image = json_encode($images);
        }
        $contact->save();
        return redirect()->route('other.contact');

    }
}
