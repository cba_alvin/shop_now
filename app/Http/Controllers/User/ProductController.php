<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\CategoryProduct;
use App\Models\Product;

class ProductController extends Controller
{
    public function index()
    {
        $categoryIndex = CategoryProduct::select()->whereNotIn('parent_id', [0])->where('pin_index_status', 1)->get();
        $productnew    = Product::select()->limit(12)->get();
        return view('user.home', compact('categoryIndex', 'productnew'));
    }
    public function singleProduct($id)
    {
        $product               = Product::select()->where('id', $id)->where('status', 1)->First();
        $productInThisCategory = $product->category->product;
        $arrayNumberToWords    = ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten'];
        if ($product) {
            $product->setview();
            return view('user.product.product', compact('product', 'arrayNumberToWords', 'productInThisCategory'));
        }
        $status = json_encode(['type' => 'danger', 'message' => 'Can not find product!']);
        return redirect()->route('home')->with('status', $status);

    }

    public function category($id = null)
    {
        $allcategory = CategoryProduct::select()->whereNotIn('parent_id', [0])->get();
        if ($id) {
            $category = CategoryProduct::select()->whereNotIn('parent_id', [0])->where('id', $id)->First();
            if ($category) {
                $data = $category->product;
                $category->setview();
                return view('user.product.category', compact('category', 'data', 'id', 'allcategory'));
            }
            $status = json_encode(['type' => 'danger', 'message' => 'Can not find category!']);
            return redirect()->route('home')->with('status', $status);
        } else {
            $data = Product::all();
            return view('user.product.category', compact('data', 'allcategory'));
        }

    }
}
