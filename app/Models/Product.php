<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;

class Product extends Model
{
    use HasRoles;
    protected $guard_name = 'web';

    public function category()
    {
        return $this->belongsTo('App\Models\CategoryProduct', 'category_product_id');
    }
    public function views()
    {
        return $this->morphMany('App\Models\Viewslog', 'viewsable');
    }
    public function setview($content = null)
    {
        $views          = new Viewslog;
        $views->content = $content ? $content : "Views Default";
        $this->views()->save($views);
    }
}
