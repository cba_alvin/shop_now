<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;

class CategoryProduct extends Model
{
    use HasRoles;

    protected $guard_name = 'web'; // or whatever guard you want to use

    public function parent()
    {
        return $this->belongsTo('App\Models\CategoryProduct', 'parent_id');
    }

    public function children()
    {
        return $this->hasMany('App\Models\CategoryProduct', 'parent_id');
    }

    public function product()
    {
        return $this->hasMany('App\Models\Product', 'category_product_id')->orderBy("created_at", "DESC");
    }
    public function views()
    {
        return $this->morphMany('App\Models\Viewslog', 'viewsable');
    }
    public function setview($content = null)
    {
        $views          = new Viewslog;
        $views->content = $content ? $content : "Views Default";
        $this->views()->save($views);
    }
}
