<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DetailTypeByCategoryProduct extends Model
{
    public function views()
    {
        return $this->morphMany('App\Models\Viewslog', 'viewsable');
    }
}
