<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    public function views()
    {
        return $this->morphMany('App\Models\Viewslog', 'viewsable');
    }
    public function setview($content = null)
    {
        $views          = new Viewslog;
        $views->content = $content ? $content : "Views Default";
        $this->views()->save($views);
    }
}
