<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Viewslog extends Model
{
    public function viewsable()
    {
        return $this->morphTo();
    }
}
