@if ($paginator->hasPages())
    <div class="btn-group mb-2">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <button type="button" class="btn btn-light waves-effect disabled">&lsaquo;</button>
        @else
            <button type="button" class="btn btn-light waves-effect"><a href="{{ $paginator->previousPageUrl() }}" rel="prev">&lsaquo;</a></button>
        @endif

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
            <button type="button" class="btn btn-light waves-effect disabled">{{ $element }}</button>
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <button type="button" class="btn btn-primary waves-effect waves-light">{{ $page }}</button>
                    @else
                        <li class="page-item"><a href="{{ $url }}">{{ $page }}</a></li>
                        <button type="button" class="btn btn-light waves-effect"><a href="{{ $url }}">{{ $page }}</a></button>
                    @endif
                @endforeach
            @endif
        @endforeach

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <button type="button" class="btn btn-light waves-effect"><a href="{{ $paginator->nextPageUrl() }}" rel="next">&rsaquo;</a></button>
        @else
            <button type="button" class="btn btn-light waves-effect disabled">&rsaquo;</button>

        @endif
    </div>
@endif
