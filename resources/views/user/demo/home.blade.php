@extends('user.layout.master')
@section('hero-slider')
@include('user.layout.include.slider')
@stop
@section('content')
<!-- Popular Brands-->
<section class="container-fluid padding-top-3x pb-5">
  <h3 class="text-center mb-30">Featured Products</h3>
  <div class="row">
    <!-- Fetured Products-->
    <div class="col-xl-12 col-md-12">
      <div class="row">
        <!-- Item-->
        <div class="col-xl-3 col-lg-4 col-sm-6">
          <div class="product-card mb-30">
            <div class="product-card-thumb"> <span class="product-badge text-danger">Sale</span><a class="product-card-link" href="shop-single.html"></a><img src="frontend/img/shop/th01.jpg" alt="Product">
              <div class="product-card-buttons">
                <button class="btn btn-white btn-sm btn-wishlist" data-toggle="tooltip" title="Wishlist"><i class="material-icons favorite_border"></i></button>
                <button class="btn btn-primary btn-sm" data-toast data-toast-type="success" data-toast-position="topRight" data-toast-icon="material-icons check" data-toast-title="Product" data-toast-message="successfuly added to cart!">Add to Cart</button>
              </div>
            </div>
            <div class="product-card-details">
              <h3 class="product-card-title"><a href="shop-single.html">Storage Box</a></h3>
              <h4 class="product-card-price">
                <del>$49.00</del>$38.00
              </h4>
            </div>
          </div>
        </div>
        <!-- Item-->
        <div class="col-xl-3 col-lg-4 col-sm-6">
          <div class="product-card mb-30">
            <div class="product-card-thumb"><a class="product-card-link" href="shop-single.html"></a><img src="frontend/img/shop/th05.jpg" alt="Product">
              <div class="product-card-buttons">
                <button class="btn btn-white btn-sm btn-wishlist" data-toggle="tooltip" title="Wishlist"><i class="material-icons favorite_border"></i></button>
                <button class="btn btn-primary btn-sm" data-toast data-toast-type="success" data-toast-position="topRight" data-toast-icon="material-icons check" data-toast-title="Product" data-toast-message="successfuly added to cart!">Add to Cart</button>
              </div>
            </div>
            <div class="product-card-details">
              <h3 class="product-card-title"><a href="shop-single.html">Lounge Chair</a></h3>
              <h4 class="product-card-price">$1420.00</h4>
            </div>
          </div>
        </div>
        <!-- Item-->
        <div class="col-xl-3 col-lg-4 col-sm-6">
          <div class="product-card mb-30">
            <div class="product-card-thumb"><span class="product-rating text-warning"><i class="material-icons star"></i><i class="material-icons star"></i><i class="material-icons star"></i><i class="material-icons star_half"></i><i class="material-icons star_border"></i></span><a class="product-card-link" href="shop-single.html"></a><img src="frontend/img/shop/th16.jpg" alt="Product">
              <div class="product-card-buttons">
                <button class="btn btn-white btn-sm btn-wishlist" data-toggle="tooltip" title="Wishlist"><i class="material-icons favorite_border"></i></button>
                <button class="btn btn-primary btn-sm" data-toast data-toast-type="success" data-toast-position="topRight" data-toast-icon="material-icons check" data-toast-title="Product" data-toast-message="successfuly added to cart!">Add to Cart</button>
              </div>
            </div>
            <div class="product-card-details">
              <h3 class="product-card-title"><a href="shop-single.html">Navy Box Bench</a></h3>
              <h4 class="product-card-price">$75.00</h4>
            </div>
          </div>
        </div>
        <!-- Item-->
        <div class="col-xl-3 col-lg-4 col-sm-6">
          <div class="product-card mb-30">
            <div class="product-card-thumb"><span class="product-badge text-muted">Out of stock</span><a class="product-card-link" href="shop-single.html"></a><img src="frontend/img/shop/th06.jpg" alt="Product">
              <div class="product-card-buttons">
                <button class="btn btn-white btn-sm btn-wishlist" data-toggle="tooltip" title="Wishlist"><i class="material-icons favorite_border"></i></button><a class="btn btn-white btn-sm" hre="shop-single.html">View Details</a>
              </div>
            </div>
            <div class="product-card-details">
              <h3 class="product-card-title"><a href="shop-single.html">LED Lighting</a></h3>
              <h4 class="product-card-price">$130.00</h4>
            </div>
          </div>
        </div>
        <!-- Item-->
        <div class="col-xl-3 col-lg-4 col-sm-6">
          <div class="product-card mb-30">
            <div class="product-card-thumb"><a class="product-card-link" href="shop-single.html"></a><img src="frontend/img/shop/th09.jpg" alt="Product">
              <div class="product-card-buttons">
                <button class="btn btn-white btn-sm btn-wishlist" data-toggle="tooltip" title="Wishlist"><i class="material-icons favorite_border"></i></button>
                <button class="btn btn-primary btn-sm" data-toast data-toast-type="success" data-toast-position="topRight" data-toast-icon="material-icons check" data-toast-title="Product" data-toast-message="successfuly added to cart!">Add to Cart</button>
              </div>
            </div>
            <div class="product-card-details">
              <h3 class="product-card-title"><a href="shop-single.html">Campfire Table</a></h3>
              <h4 class="product-card-price">$1087.00</h4>
            </div>
          </div>
        </div>
        <!-- Item-->
        <div class="col-xl-3 col-lg-4 col-sm-6">
          <div class="product-card mb-30">
            <div class="product-card-thumb"><span class="product-rating text-warning"><i class="material-icons star"></i><i class="material-icons star"></i><i class="material-icons star"></i><i class="material-icons star"></i><i class="material-icons star_border"></i></span><a class="product-card-link" href="shop-single.html"></a><img src="frontend/img/shop/th08.jpg" alt="Product">
              <div class="product-card-buttons">
                <button class="btn btn-white btn-sm btn-wishlist" data-toggle="tooltip" title="Wishlist"><i class="material-icons favorite_border"></i></button>
                <button class="btn btn-primary btn-sm" data-toast data-toast-type="success" data-toast-position="topRight" data-toast-icon="material-icons check" data-toast-title="Product" data-toast-message="successfuly added to cart!">Add to Cart</button>
              </div>
            </div>
            <div class="product-card-details">
              <h3 class="product-card-title"><a href="shop-single.html">Tissue Holder</a></h3>
              <h4 class="product-card-price">$76.40</h4>
            </div>
          </div>
        </div>
        <!-- Item-->
        <div class="col-xl-3 col-lg-4 col-sm-6">
          <div class="product-card mb-30">
            <div class="product-card-thumb"> <span class="product-badge text-danger">Sale</span><a class="product-card-link" href="shop-single.html"></a><img src="frontend/img/shop/th10.jpg" alt="Product">
              <div class="product-card-buttons">
                <button class="btn btn-white btn-sm btn-wishlist" data-toggle="tooltip" title="Wishlist"><i class="material-icons favorite_border"></i></button>
                <button class="btn btn-primary btn-sm" data-toast data-toast-type="success" data-toast-position="topRight" data-toast-icon="material-icons check" data-toast-title="Product" data-toast-message="successfuly added to cart!">Add to Cart</button>
              </div>
            </div>
            <div class="product-card-details">
              <h3 class="product-card-title"><a href="shop-single.html">Pendant Lamp</a></h3>
              <h4 class="product-card-price">
                <del>$54.00</del>$27.00
              </h4>
            </div>
          </div>
        </div>
        <!-- Item-->
        <div class="col-xl-3 col-lg-4 col-sm-6">
          <div class="product-card mb-30">
            <div class="product-card-thumb"><a class="product-card-link" href="shop-single.html"></a><img src="frontend/img/shop/th13.jpg" alt="Product">
              <div class="product-card-buttons">
                <button class="btn btn-white btn-sm btn-wishlist" data-toggle="tooltip" title="Wishlist"><i class="material-icons favorite_border"></i></button>
                <button class="btn btn-primary btn-sm" data-toast data-toast-type="success" data-toast-position="topRight" data-toast-icon="material-icons check" data-toast-title="Product" data-toast-message="successfuly added to cart!">Add to Cart</button>
              </div>
            </div>
            <div class="product-card-details">
              <h3 class="product-card-title"><a href="shop-single.html">Office Chair</a></h3>
              <h4 class="product-card-price">$329.00</h4>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

   <!-- Top Categories-->
<section class="container padding-top-3x padding-bottom-3x">
  <h3 class="text-center mb-30">Top Categories</h3>
  <div class="row">
    <div class="col-md-3 col-sm-6 mb-30"><a class="category-card flex-wrap text-center pt-0" href="shop-boxed-ls.html">
        <div class="category-card-thumb w-100"><img src="frontend/img/shop/categories/03.jpg" alt="Category"></div>
        <div class="category-card-info w-100">
          <h3 class="category-card-title">Seating</h3>
          <h4 class="category-card-subtitle">Starting from $269.00</h4>
        </div></a></div>
    <div class="col-md-3 col-sm-6 mb-30"><a class="category-card flex-wrap text-center pt-0" href="shop-boxed-ls.html">
        <div class="category-card-thumb w-100"><img src="frontend/img/shop/categories/04.jpg" alt="Category"></div>
        <div class="category-card-info w-100">
          <h3 class="category-card-title">Cabinets</h3>
          <h4 class="category-card-subtitle">Starting from $220.00</h4>
        </div></a></div>
    <div class="col-md-3 col-sm-6 mb-30"><a class="category-card flex-wrap text-center pt-0" href="shop-boxed-ls.html">
        <div class="category-card-thumb w-100"><img src="frontend/img/shop/categories/05.jpg" alt="Category"></div>
        <div class="category-card-info w-100">
          <h3 class="category-card-title">Tables</h3>
          <h4 class="category-card-subtitle">Starting from $198.00</h4>
        </div></a></div>
    <div class="col-md-3 col-sm-6 mb-30"><a class="category-card flex-wrap text-center pt-0" href="shop-boxed-ls.html">
        <div class="category-card-thumb w-100"><img src="frontend/img/shop/categories/02.jpg" alt="Category"></div>
        <div class="category-card-info w-100">
          <h3 class="category-card-title">Lighting</h3>
          <h4 class="category-card-subtitle">Starting from $95.99</h4>
        </div></a></div>
  </div>
  <div class="text-center"><a class="btn btn-outline-secondary mb-0" href="shop-categories.html">All Categories</a></div>
</section>
@stop
@section('script')
<script>
@if(session('status'))
<?php
$dataStatus = json_decode(session('status'), true);
?>
  iziToast.show({
      class: "iziToast-" + '{{$dataStatus['type']}}' || "",
      title: '',
      message: '{{$dataStatus['message']}}' || "toast message",
      animateInside: !1,
      position: "bottomRight",
      progressBar: !1,
      icon: 'iziToast-icon material-icons block',
      timeout: 3200,
      transitionIn: "fadeInLeft",
      transitionOut: "fadeOut",
      transitionInMobile: "fadeIn",
      transitionOutMobile: "fadeOut"
  })
@endif
</script>
@stop
