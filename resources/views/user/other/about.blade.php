@extends('user.layout.master')
@section('content')
<div class="container padding-bottom-2x mb-4">
  <div class="row justify-content-center">
    <div class="col-lg-10">
      <!-- Image Carousel-->
      <div class="owl-carousel" data-owl-carousel="{ &quot;nav&quot;: true, &quot;dots&quot;: true, &quot;loop&quot;: true }">
        @foreach(json_decode($about->json_image_details,true) as $key => $image)
        <figure><img src="{{ URL::asset($image['name'])}}" alt="Image"></figure>
          @endforeach
      </div>
      <!-- Post Content     -->
      <h2 class="margin-top-2x">{{$about->title}}</h2>
      <p>{!!nl2br($about->discription)!!}</p>
    </div>
  </div>
</div>
@stop
