@extends('user.layout.master')
@section('content')
<div class="container padding-bottom-2x">
  <div class="row">
    <div class="col-lg-12 col-md-12 order-md-2">
    <form action="" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
                            {{ csrf_field() }}
      <h6 class="text-muted text-normal text-uppercase margin-top-2x">Contact</h6>
      <hr class="margin-bottom-1x">
      <div class="form-group row">
        <label class="col-2 col-form-label text-muted" for="text-input">Full Name</label>
        <div class="col-10">
          <input class="form-control" type="text" name="name" id="text-input" placeholder="Full name">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-2 col-form-label text-muted" for="email-input">Email</label>
        <div class="col-10">
          <input class="form-control" type="email" name="email" id="email-input" placeholder="email@example.com">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-2 col-form-label text-muted" for="tel-input">Phone</label>
        <div class="col-10">
          <input class="form-control" type="tel" name="phone" id="tel-input" placeholder="0962123123">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-2 col-form-label text-muted" for="textarea-input">Content</label>
        <div class="col-10">
          <textarea class="form-control text-muted" name="content" id="textarea-input" rows="5"></textarea>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-2 col-form-label text-muted" for="file-input1">Image</label>
        <div class="col-10">
          <div class="custom-file">
            <input type="file" data-max-size="1028" class="imageUpload" multiple name="image[]" accept="image/*">
          </div>
          <p class="danger">*I just want take five first photo</p>
        </div>
      </div>
      <div class="form-group row">
        <div class="col-10">
          <button class="btn btn-primary margin-top-none" type="submit">Submit</button>
        </div>
      </div>
    </div>
    </form>
  </div>
</div>
@stop
@section('script')
<script>
  $(function(){
    $(".imageUpload").change(function(){
        var $fileUpload = $("input[type='file']");
        if (parseInt($fileUpload.get(0).files.length)>5){
         alert("You can only upload a maximum of 5 files");
         $(".imageUpload").val('');
        }
    });
});
</script>
@stop
