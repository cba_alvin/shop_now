@extends('user.layout.master')
@section('content')
<div class="page-title">
  <div class="container">
    <h1>{{isset($id)?$category->name:"All Category"}}</h1>
    <ul class="breadcrumbs">
      <li><a href="{{route('home')}}">Home</a>
      </li>
      <li class="separator">&nbsp;/&nbsp;</li>
      <li>{{isset($id)?$category->name:"All Category"}}</li>
    </ul>
  </div>
</div>
<!-- Page Content-->
<!-- Filters-->
<div class="container">
  <div class="d-flex flex-wrap flex-md-nowrap justify-content-center justify-content-md-between pb-3">
    <!-- Nav filters-->
    <ul class="nav-filters text-center text-md-left pb-3">
      <li @if(!isset($id)) class="active" @endif><a href="{{route('product.category')}}">All</li></a>
      @foreach($allcategory as $itemcategory)
      <li class="{{ isset($id) && $itemcategory->id == $id ? 'active' :''}}" >
        <a href="{{route('product.category',['id'=>$itemcategory->id])}}">{{$itemcategory->name}}</a>
        <sup>{{$itemcategory->product->count()}}</sup>
      </li>
      @endforeach
    </ul>
  </div>
</div>
<!-- Products Grid-->
<div class="container-fluid padding-bottom-3x mb-1">
  <div class="row mb-2">
    @if($data)
      @foreach($data as $item)
      <!-- Item-->
      <div class="col-xl-3 col-lg-4 col-sm-6">
        <div class="product-card mb-30">
          <div class="product-card-thumb"> @if($item->price_sale < $item->price) <span class="product-badge text-danger">Sale</span> @endif<a class="product-card-link" href="{{route('product.single',['id'=>$item->id])}}"></a><img src="{{ URL::asset($item->image_index)}}" alt="Product">
          </div>
          <div class="product-card-details">
            <!-- <h3 class="product-card-title"><a href="{{route('product.single',['id'=>$item->id])}}">Storage Box</a></h3> -->
            <h4 class="product-card-price">
              <del class='text-muted'>{{number_format($item->price,0,",",".")}} VND</del> {{number_format($item->price_sale,0,",",".")}} VND
            </h4>
          </div>
        </div>
      </div>
      @endforeach
    @endif
  <!-- Pagination-->
 <!--  <nav class="pagination">
    <div class="column">
      <ul class="pages">
        <li class="active"><a href="#">1</a></li>
        <li><a href="#">2</a></li>
        <li><a href="#">3</a></li>
        <li><a href="#">4</a></li>
        <li>...</li>
        <li><a href="#">12</a></li>
      </ul>
    </div>
    <div class="column text-right hidden-xs-down"><a class="btn btn-outline-secondary btn-sm" href="#">Next&nbsp;<i class="material-icons keyboard_arrow_right"></i></a></div>
  </nav> -->
</div>
@stop
