@extends('user.layout.master')
@section('content')
<div class="bg-secondary pb-4 padding-top-3x">
  <div class="container">
    <ul class="breadcrumbs">
      <li><a href="{{route('home')}}">Home</a>
      </li>
      <li class="separator">&nbsp;/&nbsp;</li>
      <li>{{$product->category->name}}</li>
      <li class="separator">&nbsp;/&nbsp;</li>
      <li>{{$product->name}}</li>
    </ul>
    <div class="row">
      <!-- Product Gallery-->
      <div class="col-md-6 mb-30">
       <div class="product-gallery">@if($product->price_sale < $product->price) <span class="product-badge text-danger">Sale</span> @endif
          @if(isset(json_decode($product->json_image_details,true)[1]['name']))
          <div class="product-carousel owl-carousel gallery-wrapper">
          @foreach(json_decode($product->json_image_details,true) as $key => $image)
            <div class="gallery-item" data-hash="{{$arrayNumberToWords[$key]}}"><a href="{{ URL::asset($image['name'])}}" data-size="555x480"><img src="{{ URL::asset($image['name'])}}" alt="Product"></a></div>
          @endforeach
          </div>
          <ul class="product-thumbnails">
          @foreach(json_decode($product->json_image_details,true) as $key => $image)
            <li class="active"><a href="#{{$arrayNumberToWords[$key]}}"><img src="{{ URL::asset($image['name'])}}" alt="Product"></a></li>
          @endforeach
          </ul>
          @endif
        </div>
      </div>
      <!-- Product Info-->
      <div class="col-md-6 mb-30">
        <div class="card border-default bg-white pt-2 box-shadow">
          <div class="card-body">
            <h2 class="mb-3">{{$product->name}}</h2>
            <h3 class="text-normal"><del class='text-muted'>{{number_format($product->price,0,",",".")}} VND</del> {{number_format($product->price_sale,0,",",".")}} VND</h3>
            <div class="row">
              <div class="col-sm-6">
              <h3>Information</h3>
              <ul class="list-unstyled text-sm mb-8">
                @if(isset(json_decode($product->json_details,true)[1]['name']))
                @foreach(json_decode($product->json_details,true) as $information)
                <li><strong>{{$information["name"]}}:</strong> {{$information["value"]}}</li>
                @endforeach
                @endif
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container padding-bottom-3x pt-5 mb-1">
  <!-- Details-->
  <div class="row pt-2">
    <div class="col-md-12">
      <h3>Details</h3>
      <hr>
      <p class="text-sm mb-4">{!! nl2br($product->discription)!!}</p>
    </div>

  </div>
  <!-- Related Products Carousel-->
  <h3 class="text-center padding-top-2x mt-2 padding-bottom-1x">You May Also Like</h3>
  <!-- Carousel-->
  <div class="owl-carousel" data-owl-carousel="{ &quot;nav&quot;: false, &quot;dots&quot;: true, &quot;margin&quot;: 30, &quot;responsive&quot;: {&quot;0&quot;:{&quot;items&quot;:1},&quot;576&quot;:{&quot;items&quot;:2},&quot;768&quot;:{&quot;items&quot;:3},&quot;991&quot;:{&quot;items&quot;:4},&quot;1200&quot;:{&quot;items&quot;:4}} }">

    <!-- Product-->
    <div class="product-card">
      <div class="product-card-thumb"> <span class="product-badge text-danger">Sale</span><a class="product-card-link" href="shop-single.html"></a><img src="frontend/img/shop/th10.jpg" alt="Product">
        <div class="product-card-buttons">
          <button class="btn btn-white btn-sm btn-wishlist" data-toggle="tooltip" title="Wishlist"><i class="material-icons favorite_border"></i></button>
          <button class="btn btn-primary btn-sm" data-toast data-toast-type="success" data-toast-position="topRight" data-toast-icon="material-icons check" data-toast-title="Product" data-toast-message="successfuly added to cart!">Add to Cart</button>
        </div>
      </div>
      <div class="product-card-details">
        <h3 class="product-card-title"><a href="shop-single.html">Pendant Lamp</a></h3>
        <h4 class="product-card-price">
          <del>$54.00</del>$27.00
        </h4>
      </div>
    </div>

  </div>
</div>

<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="pswp__bg"></div>
  <div class="pswp__scroll-wrap">
    <div class="pswp__container">
      <div class="pswp__item"></div>
      <div class="pswp__item"></div>
      <div class="pswp__item"></div>
    </div>
    <div class="pswp__ui pswp__ui--hidden">
      <div class="pswp__top-bar">
        <div class="pswp__counter"></div>
        <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
        <!-- <button class="pswp__button pswp__button--share" title="Share"></button> -->
        <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
        <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
        <div class="pswp__preloader">
          <div class="pswp__preloader__icn">
            <div class="pswp__preloader__cut">
              <div class="pswp__preloader__donut"></div>
            </div>
          </div>
        </div>
      </div>
      <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
        <div class="pswp__share-tooltip"></div>
      </div>
      <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button>
      <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"></button>
      <div class="pswp__caption">
        <div class="pswp__caption__center"></div>
      </div>
    </div>
  </div>
</div>
@stop
