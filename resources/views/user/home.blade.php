@extends('user.layout.master')
@section('hero-slider')
@include('user.layout.include.slider')
@stop
@section('content')
<!-- Popular Brands-->
<section class="container-fluid padding-top-3x pb-5">
  <h3 class="text-center mb-30">New Products</h3>
  <div class="row">
    <!-- Fetured Products-->
    <div class="col-xl-12 col-md-12">
      <div class="row">
        @if($productnew)
        @foreach($productnew as $product)
        <!-- Item-->
        <div class="col-xl-3 col-lg-4 col-sm-6">
          <div class="product-card mb-30">
            <div class="product-card-thumb">@if($product->price_sale < $product->price) <span class="product-badge text-danger">Sale</span> @endif<a class="product-card-link" href="{{route('product.single',['id'=>$product->id])}}"></a><img src="{{ URL::asset($product->image_index)}}" alt="Product">
              <!-- <div class="product-card-buttons">
                <button class="btn btn-white btn-sm btn-wishlist" data-toggle="tooltip" title="Wishlist"><i class="material-icons favorite_border"></i></button>
                <button class="btn btn-primary btn-sm" data-toast data-toast-type="success" data-toast-position="topRight" data-toast-icon="material-icons check" data-toast-title="Product" data-toast-message="successfuly added to cart!">Add to Cart</button>
              </div> -->
            </div>
            <div class="product-card-details">
              <h4 class="product-card-price">
                <del class='text-muted'>{{number_format($product->price,0,",",".")}} VND</del> {{number_format($product->price_sale,0,",",".")}} VND
              </h4>
            </div>
          </div>
        </div>
        @endforeach
        @endif
      </div>
    </div>
  </div>
</section>

   <!-- Top Categories-->
<section class="container padding-top-3x padding-bottom-3x">
  <h3 class="text-center mb-30">Top Categories</h3>
  <div class="row">
    @if($categoryIndex)
    @foreach($categoryIndex as $categoryIndexItem)
    <div class="col-md-3 col-sm-6 mb-30"><a class="category-card flex-wrap text-center pt-0" href="{{route('product.category',['id'=>$categoryIndexItem->id])}}">
        <div class="category-card-thumb w-100"><img src="{{ URL::asset($categoryIndexItem->image)}}" alt="Category"></div>
        <div class="category-card-info w-100">
          <h3 class="category-card-title">{{$categoryIndexItem->name}}</h3>
        </div></a>
      </div>
    @endforeach
    @endif
  </div>
  <div class="text-center"><a class="btn btn-outline-secondary mb-0" href="shop-categories.html">All Categories</a></div>
</section>
@stop
@section('script')
<script>
@if(session('status'))
<?php
$dataStatus = json_decode(session('status'), true);
?>
  iziToast.show({
      class: "iziToast-" + '{{$dataStatus['type']}}' || "",
      title: '',
      message: '{{$dataStatus['message']}}' || "toast message",
      animateInside: !1,
      position: "bottomRight",
      progressBar: !1,
      icon: 'iziToast-icon material-icons block',
      timeout: 3200,
      transitionIn: "fadeInLeft",
      transitionOut: "fadeOut",
      transitionInMobile: "fadeIn",
      transitionOutMobile: "fadeOut"
  })
@endif
</script>
@stop
