<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>ShopNow Demo
    </title>
    <!-- SEO Meta Tags-->
    <meta name="description" content="ShopNow - Demo">
    <meta name="keywords" content="shop, e-commerce,online store, business">
    <meta name="author" content="Alvin">
    <!-- Mobile Specific Meta Tag-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <!-- Favicon and Apple Icons-->
    <link rel="icon" type="image/x-icon" href="{{ URL::asset('frontend/favicon.ico')}}">
    <link rel="icon" type="image/png" href="{{ URL::asset('frontend/favicon.png')}}">
    <link rel="apple-touch-icon" href="{{ URL::asset('frontend/touch-icon-iphone.png')}}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ URL::asset('frontend/touch-icon-ipad.png')}}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ URL::asset('frontend/touch-icon-iphone-retina.png')}}">
    <link rel="apple-touch-icon" sizes="167x167" href="{{ URL::asset('frontend/touch-icon-ipad-retina.png')}}">
    <!-- Vendor Styles including: Bootstrap, Font Icons, Plugins, etc.-->
    <link rel="stylesheet" media="screen" href="{{ URL::asset('frontend/css/vendor.min.css')}}">
    <!-- Main Template Styles-->
    <link id="mainStyles" rel="stylesheet" media="screen" href="{{ URL::asset('frontend/css/styles.min.css')}}">
    <!-- Customizer Styles-->
    <link rel="stylesheet" media="screen" href="{{ URL::asset('frontend/customizer/customizer.min.css')}}">
    <!-- Google Tag Manager-->
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-115209630-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-115209630-2');
</script>

    @yield('head')
    <!-- Modernizr-->
    <script src="{{ URL::asset('frontend/js/modernizr.min.js')}}"></script>
  </head>
  <!-- Body-->
  <body>
    <!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5ad568fc227d3d7edc2401a6/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
    <!-- Navbar-->
    <!-- Remove "navbar-sticky" class to make navigation bar scrollable with the page.-->
   @include('user.layout.include.header')
    <!-- Page Content-->
    <!-- Hero Slider-->
    @yield('hero-slider')

    @yield('content')
    <!-- Site Footer-->
   @include('user.layout.include.footer')
    <!-- Back To Top Button--><a class="scroll-to-top-btn" href="#"><i class="material-icons trending_flat"></i></a>
    <!-- Backdrop-->
    <div class="site-backdrop"></div>
    <!-- JavaScript (jQuery) libraries, plugins and custom scripts-->
    <script src="{{ URL::asset('frontend/js/vendor.min.js')}}"></script>
    <script src="{{ URL::asset('frontend/js/scripts.min.js')}}"></script>
    <!-- Customizer scripts-->
    <script src="{{ URL::asset('frontend/customizer/customizer.min.js')}}"></script>
    <script>
var delay = (function(){
  var timer = 0;
  return function(callback, ms){
    clearTimeout (timer);
    timer = setTimeout(callback, ms);
  };
})();
 URLdomain = "{{ URL::asset('')}}"
 URLproduct = "{{route('product.single')}}"
 console.log(URLdomain)
    $('.searchKey').keyup(function() {
    delay(function(){
        console.log($('.searchKey').val());
        $.ajax({
            url: "{{route('product.search')}}?key="+$('.searchKey').val(),
            error: function(){
                // will fire when timeout is reached
            },
            success: function(){
                prodcut='<div class="entry"><div class="entry-thumb"><a href="shop-single.html"><img src="frontend/img/shop/widget/01.png" alt="Product"></a></div><div class="entry-content"><h4 class="entry-title"><a href="shop-single.html">Max <span class="text-highlighted">Task Chair</span></a></h4><span class="entry-meta">$299.00</span></div></div>'
                $('.SearchProductKeyUp').append(product);
                product.show('slow');
            }
        });
    }, 1000 );
    });
    </script>
    @yield('script')
  </body>
</html>
