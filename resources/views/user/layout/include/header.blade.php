 <header class="navbar navbar-sticky">
  <!-- Site Branding-->
  <div class="site-branding"><h1><a class="site-logo hidden-xs-down" href="{{route('home')}}"><img src="{{ URL::asset('frontend/img/logo/logo.png')}}" alt="Unishop"></a></h1>
    </div>
  </div>
  <!-- Main Navigation-->
  <nav class="site-menu">
    <ul>
      <li class="active"><a href="{{route('home')}}"><span>Home</span></a>
      </li>
      <li><a href="{{route('product.category')}}"><span>Category</span></a>
        <ul class="sub-menu">
          @php
use App\Models\CategoryProduct;
            $menuCategorys = CategoryProduct::select()->whereNotIn('parent_id', [0])->get();
          @endphp
            @if($menuCategorys)
            @foreach($menuCategorys as $menuCategory)
              <li><a href="{{route('product.category',['id'=>$menuCategory->id])}}">{{$menuCategory->name}}</a></li>
            @endforeach
            @endif
        </ul>
      </li>
      <li><a href="{{route('other.contact')}}"><span>Contact</span></a>
      </li>
      <li><a href="{{route('other.about')}}"><span>About</span></a>
      </li>
    </ul>
  </nav>
<div class="toolbar">
  <div class="inner"><a class="toolbar-toggle mobile-menu-toggle" href="#mobileMenu"><i class="material-icons menu"></i></a><a class="toolbar-toggle search-toggle" href="#search"><i class="material-icons search"></i></a></div>
  <!-- Toolbar Dropdown-->
  <div class="toolbar-dropdown">
    <!-- Mobile Menu Section-->
    <div class="toolbar-section" id="mobileMenu">
      <!-- Search Box-->
      <form class="input-group form-group" method="get"><span class="input-group-btn">
          <button type="submit"><i class="material-icons search"></i></button></span>
        <input class="form-control" type="search" placeholder="Search website">
      </form>
      <!-- Slideable (Mobile) Menu-->
      <nav class="slideable-menu mt-4">
        <ul class="menu">
          <li class="has-children"><a href="{{route('home')}}"><span>Home</span></a></li>
          <li class="has-children"><span><a href="{{route('product.category')}}"><span>Category</span></a><span class="sub-menu-toggle"></span></span>
            <ul class="slideable-submenu">
              @if($menuCategorys)
            @foreach($menuCategorys as $menuCategory)
              <li><a href="{{route('product.category',['id'=>$menuCategory->id])}}">{{$menuCategory->name}}</a></li>
            @endforeach
            @endif
            </ul>
          </li>
          <li class="has-children"><a href="{{route('other.contact')}}"><span>Contact</span></a></li>
          <li class="has-children"><a href="{{route('other.about')}}"><span>About</span></a></li>
        </ul>
      </nav>
    </div>
    <!-- Search Section-->
    <div class="toolbar-section" id="search">
      <form class="search-form mb-2" method="get">
        <input type="search" class="searchKey" placeholder="Type search query"><i class="material-icons search"></i>
      </form>
      <!-- Products-->
      <h3 class="widget-title">Found in Products</h3>
      <div class="widget widget-featured-products SearchProductKeyUp">
        <div class="entry">
          <div class="entry-thumb"><a href="shop-single.html"><img src="frontend/img/shop/widget/01.png" alt="Product"></a></div>
          <div class="entry-content">
            <h4 class="entry-title"><a href="shop-single.html">Max <span class='text-highlighted'>Task Chair</span></a></h4><span class="entry-meta">$299.00</span>
          </div>
        </div>

      </div>

    </div>
  </div>
</div>
</header>
