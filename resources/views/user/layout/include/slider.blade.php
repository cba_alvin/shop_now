<section class="hero-slider">
  <div class="owl-carousel large-controls dots-inside pb-4" data-owl-carousel="{ &quot;nav&quot;: true, &quot;dots&quot;: true, &quot;loop&quot;: true, &quot;autoplay&quot;: true, &quot;autoplayTimeout&quot;: 8000 }">
    @php
    use App\Models\Banner;
    $banners = Banner::all();
    @endphp
    @if($banners)
    @foreach($banners as $banner)
    <div class="container-fluid">
      <div class="row align-items-center">
        <div class="col-md-6">
          <div class="pr-3 pt-5 pb-0 py-md-5"><img class="d-block" src="{{ URL::asset($banner->image)}}" alt="Product"></div>
        </div>
        <div class="col-xl-4 col-md-6">
          <div class="padding-top-3x padding-bottom-3x px-3 px-lg-5 text-center text-md-left from-bottom">
            <h2>{{$banner->title}}</h2>
            <p class="text-sm text-muted">{!! nl2br($banner->content) !!}</p><a class="btn btn-primary mx-0 scale-up delay-1" href="{{$banner->url}}">View Collection</a>
          </div>
        </div>
      </div>
    </div>
    @endforeach
    @endif
  </div>
</section>
