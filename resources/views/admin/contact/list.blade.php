@extends('admin.layout.master',['title'=>'List Contact'])
@section('content')
<div class="row">
    <div class="col-12">
        <div class="card-box table-responsive">
            <h4 class="m-t-0 header-title">List Contact</h4>
            <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>name</th>
                    <th>email</th>
                    <th>content</th>
                    <th>status</th>
                    <th>Created at</th>
                    <th>Updated at</th>
                    <th>Tool</th>
                </tr>
                </thead>
                <tbody>
                @if($data)
                    @foreach($data as $item)
                        <tr @if($item->views == 0) class="table-info" @endif>
                            <td>{{$item->id}}</td>
                            <td>{{$item->name}}</td>
                            <td>{{$item->email}}</td>
                            <td>{{$item->content}}</td>
                            <td>{{$item->views > 0 ? "Seen":"Read"}}</td>
                            <td>{{$item->created_at->diffForHumans()}}</td>
                            <td>{{$item->updated_at->diffForHumans()}}</td>
                            <td>
                                <a href="{{route('admin.contact.detail',['id'=>$item->id])}}" class="btn btn-success waves-light waves-effect">View</a>
                               <!--  <button type="button" class="btn btn-danger waves-light waves-effect btn-delete-item" data-id="{{$item->id}}" data-name="{{$item->title}}" data-toggle="modal" data-target=".bs-example-modal-sm">Delete</button> -->
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection

@section('modal')
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-dialog-centered modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title titleCategoryDeleteModel" id="mySmallModalLabel"></h4>
            </div>
            <div class="modal-body">
                <form action="{{route('admin.contact.delete')}}" method="post" accept-charset="utf-8">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" class="idCategoryDeleteModal">
                    <p>Do you want to delete it?</p>
                    <div class="form-group account-btn text-center m-t-10">
                        <div class="col-12">
                            <button type="button" class="btn btn-secondary waves-light waves-effect" data-dismiss="modal">Close</button>
                            <button class="btn btn-danger waves-light waves-effect" type="submit">Yes, I agree!</button>
                        </div>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endsection

@section('head')
        <link href="{{ URL::asset('adminstyle/plugins/jquery-toastr/jquery.toast.min.css')}}" rel="stylesheet" />
@endsection
@section('script')
<script src="{{ URL::asset('adminstyle/plugins/jquery-toastr/jquery.toast.min.js')}}"></script>

<script src="{{ URL::asset('adminstyle/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{ URL::asset('adminstyle/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
<!-- Buttons examples -->
<script src="{{ URL::asset('adminstyle/plugins/datatables/dataTables.buttons.min.js')}}"></script>
<script src="{{ URL::asset('adminstyle/plugins/datatables/buttons.bootstrap4.min.js')}}"></script>
<script src="{{ URL::asset('adminstyle/plugins/datatables/jszip.min.js')}}"></script>
<script src="{{ URL::asset('adminstyle/plugins/datatables/pdfmake.min.js')}}"></script>
<script src="{{ URL::asset('adminstyle/plugins/datatables/vfs_fonts.js')}}"></script>
<script src="{{ URL::asset('adminstyle/plugins/datatables/buttons.html5.min.js')}}"></script>
<script src="{{ URL::asset('adminstyle/plugins/datatables/buttons.print.min.js')}}"></script>
 <script src="{{ URL::asset('adminstyle/plugins/datatables/dataTables.keyTable.min.js')}}"></script>

<!-- Responsive examples -->
<script src="{{ URL::asset('adminstyle/plugins/datatables/dataTables.responsive.min.js')}}"></script>
<script src="{{ URL::asset('adminstyle/plugins/datatables/responsive.bootstrap4.min.js')}}"></script>

<!-- Selection table -->
<script src="{{ URL::asset('adminstyle/plugins/datatables/dataTables.select.min.js')}}"></script>
<script type="text/javascript">
$(document).ready(function() {
    @if (session('status'))
    @php
    $status = json_decode(session('status'), true);
    @endphp
    $.toast({heading:"{{ $status['status']}}!",text:"{{ $status['message']}}",
        position:"top-right",loaderBg:"#3b98b5",icon:"{{ strtolower($status['status'])}}",hideAfter:3e3,stack:1})
    @endif

    // Default Datatable
    $('#datatable').DataTable();

    //Buttons examples
    var table = $('#datatable-buttons').DataTable({
        lengthChange: false,
        buttons: ['copy', 'excel', 'pdf'],
              "order": [[ 4, 'asc' ]]
    });

    // Key Tables

    $('#key-table').DataTable({
        keys: true,
    });

    // Responsive Datatable
    $('#responsive-datatable').DataTable();

    // Multi Selection Datatable
    $('#selection-datatable').DataTable({
        select: {
            style: 'multi'
        }
    });

    table.buttons().container().appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');
});
$('.btn-delete-item').click(function(){
    var id = $(this).data('id');
    var title = $(this).data('name');
    $('.titleCategoryDeleteModel').text('Delete contact '+title);
    $(".idCategoryDeleteModal").val(id);
});
</script>
@stop
