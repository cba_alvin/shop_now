@extends('admin.layout.master',['title'=>$contact->name." | Contact"])

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card-box task-detail">
            <div class="media mt-0 m-b-30">
                <div class="media-body">
                    <h5 class="media-heading mb-0 mt-0"></h5>
                    <div class="panel-body">
                        <div class="text-left">
                            <p class="text font-20"><strong>Full Name :</strong> <span class="m-l-15">{{$contact->name}}</span></p>

                            <p class="text font-20"><strong>Phone :</strong><span class="m-l-15">{{$contact->phone}}</span></p>

                            <p class="text font-20"><strong>Email :</strong> <span class="m-l-15">{{$contact->email}}</span></p>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <p class="font-10">
           	{{$contact->content}}
            </p>
            <ul class="list-inline task-dates m-b-0 mt-5">
                <li>
                    <h5 class="m-b-5">Post Date</h5>
                    <p>{{$contact->created_at->diffForHumans()}}</p>
                </li>
            </ul>
            <div class="clearfix"></div>
            <hr>
            <div class="task-tags mt-4">
                <h5 class="">Image</h5>
                <div class="port">
                    <div class="portfolioContainer">
                    	@if(isset(json_decode($contact->image,true)[0]['patch']))
                    	@foreach(json_decode($contact->image,true) as $image)
                        <div class="col-sm-6 col-md-4 webdesign illustrator">
                            <a href="{{ URL::asset($image['patch'])}}" class="image-popup">
                                <div class="portfolio-masonry-box">
                                    <div class="portfolio-masonry-img">
                                        <img src="{{ URL::asset($image['patch'])}}" class="thumb-img img-fluid">
                                    </div>
                                </div>
                            </a>
                        </div>
                        @endforeach
                        @endif
                    </div>
                </div> <!-- End row -->
            </div>
        </div>
    </div><!-- end col -->
</div>
@stop

@section("head")
<link href="{{ URL::asset('adminstyle/plugins/magnific-popup/css/magnific-popup.css')}}" rel="stylesheet" />
@stop
@section("script")
<script type="text/javascript" src="{{ URL::asset('adminstyle/plugins/isotope/js/isotope.pkgd.min.js')}}"></script>
<script type="text/javascript" src="{{ URL::asset('adminstyle/plugins/magnific-popup/js/jquery.magnific-popup.min.js')}}"></script>
<script src="{{ URL::asset('adminstyle/plugins/bootstrap-tagsinput/js/bootstrap-tagsinput.min.js')}}"></script>
<script type="text/javascript">
    $(window).on('load', function () {
        var $container = $('.portfolioContainer');
        $container.isotope({
            filter: '*',
            animationOptions: {
                duration: 750,
                easing: 'linear',
                queue: false
            }
        });

        $('.portfolioFilter a').click(function(){
            $('.portfolioFilter .current').removeClass('current');
            $(this).addClass('current');
            var selector = $(this).attr('data-filter');
            $container.isotope({
                filter: selector,
                animationOptions: {
                    duration: 750,
                    easing: 'linear',
                    queue: false
                }
            });
            return false;
        });
    });
    $(document).ready(function() {
        $('.image-popup').magnificPopup({
            type: 'image',
            url:'2332',
            closeOnContentClick: true,
            mainClass: 'mfp-fade',
            gallery: {
            	url:'12312323',
                enabled: true,
                navigateByImgClick: true,
                preload: [0,1] // Will preload 0 - before current, and 1 after the current image
            },
        });
    });
</script>
@stop
