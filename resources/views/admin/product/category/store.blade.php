@extends('admin.layout.master',['title'=>isset($id)?"Update category ".$category->name:'Create new category'])

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card-box">
            <h4 class="m-t-0 header-title">Category {{isset($id)?"Update":'Create'}}</h4>

            <div class="row">
                <div class="col-12">
                    <div class="p-20">
                        <form class="form-horizontal" action="{{isset($id)?route('admin.products.category.store',['id'=>$id]):route('admin.products.category.store')}}" role="form" method="POST">
                            {{ csrf_field() }}
                            <div class="form-group row">
                                <label class="col-2 col-form-label">Category parent</label>
                                <div class="col-10">
                                    <select class="form-control" name="parent_id">
                                        <option>No parent</option>
                                        @if($categoryList)
                                            @foreach($categoryList as $itemCategory)
                                                <option value="{{$itemCategory->id}}">{{$itemCategory->name}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    @if(isset($category) && $category->children->count() != 0)
                                    <p class="text-muted">*There are  {{$category->children->count()}} categories that are children of this category. You can not change it!</p>
                                    @else
                                    <p class="text-muted">*If you don't select it, it defaults to the parent category</p>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-2 col-form-label">Name<span class="text-danger">*</span></label>
                                <div class="col-10">
                                    <input type="text" class="form-control" name="name" value="{{ old('name', isset($category->name)?$category->name:'') }}" placeholder="Name category product">
                                    @if($errors->has('name'))
                                    <ul class="parsley-errors-list filled" id="parsley-id-18"><li class="parsley-required">{{$errors->first('name')}}</li></ul>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row justify-content-end">
                                <label class="col-2 col-form-label">Pin on index</label>
                                <div class=" col-10">
                                    <div class="checkbox checkbox-primary">
                                        <input id="pin_index_status" type="checkbox" name="pin_index_status" {{old('pin_index_status', isset($category->pin_index_status)?$category->pin_index_status:'0') && old('pin_index_status', isset($category->pin_index_status)?$category->pin_index_status:'0') == '1'|| old('pin_index_status', isset($category->pin_index_status)?$category->pin_index_status:'0') == 'on' ?'checked':''}}>
                                        <label for="pin_index_status">
                                            <span class="pin_index_status">{{old('pin_index_status', isset($category->pin_index_status)?$category->pin_index_status:'0') && old('pin_index_status', isset($category->pin_index_status)?$category->pin_index_status:'0') == '1'|| old('pin_index_status', isset($category->pin_index_status)?$category->pin_index_status:'0') == 'on' ?'On':'Off'}}</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row justify-content-end">
                                <label class="col-2 col-form-label">Status</label>
                                <div class=" col-10">
                                    <div class="checkbox checkbox-primary">
                                        <input id="status" type="checkbox" name="status" {{old('status', isset($category->status)?$category->status:'0') && old('status', isset($category->status)?$category->status:'0') == '1'|| old('status', isset($category->status)?$category->status:'0') == 'on' ?'checked':''}}>
                                        <label for="status">
                                            <span class="status">{{old('status', isset($category->status)?$category->status:'0') && old('status', isset($category->status)?$category->status:'0') == '1'|| old('status', isset($category->status)?$category->status:'0') == 'on' ?'On':'Off'}}</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group mb-0 justify-content-end row">
                                <div class="col-9">
                                    <button type="submit" class="btn btn-info waves-effect waves-light">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
            <!-- end row -->

        </div> <!-- end card-box -->
    </div><!-- end col -->
</div>
@stop

@section('script')
<script>
    $('#pin_index_status').change(function(){
        if(this.checked)
        {
            ($('.pin_index_status')).html('On');
        }
        else
        {
            ($('.pin_index_status')).html('Off');
        }
    });
    $('#status').change(function(){
        if(this.checked)
        {
            ($('.status')).html('On');
        }
        else
        {
            ($('.status')).html('Off');
        }
    });
</script>
@stop
