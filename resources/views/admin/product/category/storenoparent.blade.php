@extends('admin.layout.master',['title'=>isset($id)?"Update category ".$category->name:'Create new category'])

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card-box">
            <h4 class="m-t-0 header-title">Category {{isset($id)?"Update":'Create'}}</h4>

            <div class="row">
                <div class="col-12">
                    <div class="p-20">
                        <form class="form-horizontal" action="{{isset($id)?route('admin.products.category.store',['id'=>$id]):route('admin.products.category.store')}}" role="form" method="POST">
                            {{ csrf_field() }}
                            <div class="form-group row">
                                <label class="col-2 col-form-label">Name<span class="text-danger">*</span></label>
                                <div class="col-10">
                                    <input type="text" class="form-control" name="name" value="{{ old('name', isset($category->name)?$category->name:'') }}" placeholder="Name category product">
                                    @if($errors->has('name'))
                                    <ul class="parsley-errors-list filled" id="parsley-id-18"><li class="parsley-required">{{$errors->first('name')}}</li></ul>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-2 col-form-label">Image<span class="text-danger">*</span></label>
                                <div class="col-10">
                                    <div class="fileupload fileupload-new ImageUploadItem" data-provides="fileupload">
                                        <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                            <img src="{{ URL::asset(old('image', isset($category->image)?$category->image:'No_image_3x4.svg'))}}" alt="image">
                                        </div>
                                        <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                        <div>
                                            <button type="button" class="btn btn-custom btn-file">
                                                <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Select image</span>
                                                <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                                                <input type="file" accept="image/*" class="btn-light btnUploadImageItem" onchange="UploadImage(this);" >
                                                <input type="hidden" class="ValueImageUpload" value="{{ old('image', isset($category->image)?$category->image:'')}}"  name="image">
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row justify-content-end">
                                <label class="col-2 col-form-label">Pin on index</label>
                                <div class=" col-10">
                                    <div class="checkbox checkbox-primary">
                                        <input id="pin_index_status" type="checkbox" name="pin_index_status" {{old('pin_index_status', isset($category->pin_index_status)?$category->pin_index_status:'0') && old('pin_index_status', isset($category->pin_index_status)?$category->pin_index_status:'0') == '1'|| old('pin_index_status', isset($category->pin_index_status)?$category->pin_index_status:'0') == 'on' ?'checked':''}}>
                                        <label for="pin_index_status">
                                            <span class="pin_index_status">{{old('pin_index_status', isset($category->pin_index_status)?$category->pin_index_status:'0') && old('pin_index_status', isset($category->pin_index_status)?$category->pin_index_status:'0') == '1'|| old('pin_index_status', isset($category->pin_index_status)?$category->pin_index_status:'0') == 'on' ?'On':'Off'}}</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row justify-content-end">
                                <label class="col-2 col-form-label">Status</label>
                                <div class=" col-10">
                                    <div class="checkbox checkbox-primary">
                                        <input id="status" type="checkbox" name="status" {{old('status', isset($category->status)?$category->status:'0') && old('status', isset($category->status)?$category->status:'0') == '1'|| old('status', isset($category->status)?$category->status:'0') == 'on' ?'checked':''}}>
                                        <label for="status">
                                            <span class="status">{{old('status', isset($category->status)?$category->status:'0') && old('status', isset($category->status)?$category->status:'0') == '1'|| old('status', isset($category->status)?$category->status:'0') == 'on' ?'On':'Off'}}</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group mb-0 justify-content-end row">
                                <div class="col-9">
                                    <button type="submit" class="btn btn-info waves-effect waves-light">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
            <!-- end row -->

        </div> <!-- end card-box -->
    </div><!-- end col -->
</div>
@stop
@section('head')
<link href="{{ URL::asset('adminstyle/plugins/bootstrap-fileupload/bootstrap-fileupload.css')}}" rel="stylesheet" />
@stop
@section('script')
<script src="{{ URL::asset('adminstyle/plugins/bootstrap-fileupload/bootstrap-fileupload.js')}}"></script>
<script src="{{ URL::asset('adminstyle/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js')}}"></script>
<script src="{{ URL::asset('adminstyle/plugins/autoNumeric/autoNumeric.js')}}"></script>
<script>
    $('#pin_index_status').change(function(){
        if(this.checked)
        {
            ($('.pin_index_status')).html('On');
        }
        else
        {
            ($('.pin_index_status')).html('Off');
        }
    });
    $('#status').change(function(){
        if(this.checked)
        {
            ($('.status')).html('On');
        }
        else
        {
            ($('.status')).html('Off');
        }
    });
    function UploadImage(e){
    var form_data = new FormData();
        form_data.append('file', e.files[0]);
        form_data.append('_token', '{{csrf_token()}}');
    $.ajax({
      url: "{{route('admin.uploads.image',['type'=>'category'])}}", // Url to which the request is send
      data: form_data,
      type: 'POST',
      contentType: false,
      processData: false,
      success: function(res)   // A function to be called if request succeeds
      {
        dataSuccess = JSON.parse(res);
        console.log(res);
        $('#image_show').attr('src', dataSuccess.filename);
        $('#images_show_data').val(dataSuccess.filename);
        $(e).next('input').val(dataSuccess.filename)
      }
    });
  };
</script>
@stop
