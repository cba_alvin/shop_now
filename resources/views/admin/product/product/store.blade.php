@extends('admin.layout.master',['title'=>isset($id)?"Update product ".$product->name:'Create new product'])

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card-box">
            <h4 class="m-t-0 header-title">{{isset($id)?"Update":'Create new'}} product</h4>

            <div class="row">
                <div class="col-12">
                    <div class="p-20">
                        <form class="form-horizontal" action="{{isset($id)?route('admin.products.product.store',['id'=>$id]):route('admin.products.product.store')}}" role="form" method="POST"  enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-group row">
                                <label class="col-2 col-form-label">Category parent</label>
                                <div class="col-10">
                                    <select class="form-control select2" name="category_product_id">
                                        @isset($categoryList[0])
                                            @foreach($categoryList as $categorylv1)
                                            <optgroup label="{{$categorylv1->name}}">
                                                @isset($categorylv1->children[0])
                                                    @foreach($categorylv1->children as $categorylv2)
                                                        <option value="{{$categorylv2->id}}" {{old('category_product_id',isset($product->category_product_id) && $product->category_product_id == $categorylv2->id ? "selected":'')}}>{{$categorylv2->name}}</option>
                                                    @endforeach
                                                @endisset
                                            </optgroup>
                                            @endforeach
                                        @endisset
                                        option
                                    </select>
                                    <p class="text-muted">*If blank type data type please go to <a href="{{route('admin.products.category.store')}}">Create Category</a></p>
                                    @if($errors->has('category_product_id'))
                                    <ul class="parsley-errors-list filled" id="parsley-id-18"><li class="parsley-required">{{$errors->first('category_product_id')}}</li></ul>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-2 col-form-label">Name<span class="text-danger">*</span></label>
                                <div class="col-10">
                                    <input type="text" class="form-control" name="name" value="{{ old('name', isset($product->name)?$product->name:'') }}" placeholder="Name product">
                                    @if($errors->has('name'))
                                    <ul class="parsley-errors-list filled" id="parsley-id-18"><li class="parsley-required">{{$errors->first('name')}}</li></ul>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-2 col-form-label">Image Upload</label>
                                <div class="col-10">
                                    <div class="ImageUpload col-12">
                                        @if(old('image', isset($product->json_image_details)?json_decode($product->json_image_details,true):null) && old('image', isset($product->json_image_details)?json_decode($product->json_image_details,true):null)[1]['name'] != null)
                                        @foreach(old('image',isset($product->json_image_details)?json_decode($product->json_image_details,true):null) as $key => $value)
                                        @if($value['name'] != null)
                                            <div class="fileupload fileupload-new ImageUploadItem col-auto" data-provides="fileupload">
                                                <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                                    <img src="{{ URL::asset($value['name'])}}" alt="image">
                                                </div>
                                                <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                                <div>
                                                    <button type="button" class="btn btn-custom btn-file">
                                                        <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Select image</span>
                                                        <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                                                        <input type="file" class="btn-light btnUploadImageItem" onchange="UploadImage(this);" accept="image/x-png,image/gif,image/jpeg">
                                                        <input type="hidden" class="ValueImageUpload"  name="image[{{$key}}][name]" value="{{$value['name']}}">
                                                    </button>
                                                    @if($key == '1')
                                                        <button type="button" class="btn btn-info btn-plus-image"> <i class="fa fa-plus-circle"></i> </button>
                                                    @else
                                                        <button type="button" class="btn btn-info btn-plus-image"> <i class="fa fa-plus-circle"></i> </button>
                                                        <button type="button" class="btn btn-danger btn-delete-image-item"> <i class="fa fa-times-circle-o"></i> </button>
                                                    @endif
                                                </div>
                                            </div>
                                        @endif
                                        @endforeach
                                        @else
                                        <div class="fileupload fileupload-new ImageUploadItem col-auto" data-provides="fileupload">
                                            <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                                <img src="https://upload.wikimedia.org/wikipedia/commons/6/6c/No_image_3x4.svg" alt="image">
                                            </div>
                                            <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                            <div>
                                                <button type="button" class="btn btn-custom btn-file">
                                                    <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Select image</span>
                                                    <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                                                    <input type="file" class="btn-light btnUploadImageItem" onchange="UploadImage(this);" accept="image/x-png,image/gif,image/jpeg" >
                                                    <input type="hidden" class="ValueImageUpload"  name="image[1][name]">
                                                </button>
                                                <button type="button" class="btn btn-info btn-plus-image"> <i class="fa fa-plus-circle"></i> </button>
                                            </div>
                                        </div>
                                        @endif
                                    </div>
                                    @if($errors->has('image'))
                                    <ul class="parsley-errors-list filled" id="parsley-id-18"><li class="parsley-required">{{$errors->first('image')}}</li></ul>
                                    @endif
                                </div>
                            </div>

                             <div class="form-group row">
                                <label class="col-2 col-form-label">Price<span class="text-danger">*</span> <br>(Sell-Through : <span id="rate"></span>%)</label>
                                <div class="col-10">
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="inputEmail4" class="col-form-label">Cost</label>
                                            <input type="text" class="form-control autonumber" id="price" name="price" data-v-max="9999999999" data-v-min="0" value="{{old('price',isset($product->price)?$product->price:'')}}" placeholder="cost">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="inputPassword4" class="col-form-label">Promotional price</label>
                                            <input type="text" class="form-control autonumber" id="price_sale" name="price_sale" data-v-max="9999999999" value="{{old('price_sale',isset($product->price_sale)?$product->price_sale:'')}}" name="price_sale" placeholder="Promotional price">
                                        </div>
                                    </div>
                                    @if($errors->has('price'))
                                    <ul class="parsley-errors-list filled" id="parsley-id-18"><li class="parsley-required">{{$errors->first('price')}}</li></ul>
                                    @endif
                                    @if($errors->has('price_sale'))
                                    <ul class="parsley-errors-list filled" id="parsley-id-18"><li class="parsley-required">{{$errors->first('price_sale')}}</li></ul>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-2 col-form-label">Detail product</label>
                                <div class="col-10">
                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            <label for="inputEmail4" class="col-form-label">Information</label>
                                            <div class="boxInformation">
                                                @if(old('detail',isset($product->json_details)?json_decode($product->json_details,true):null) && old('detail',isset($product->json_details)?json_decode($product->json_details,true):null)[1]['name'] != null)
                                                    @foreach(old('detail',isset($product->json_details)?json_decode($product->json_details,true):null) as $key => $value)
                                                        @if($value['name'] != null)
                                                        <div class="form-row align-items-center box_details">
                                                            <div class="col-auto">
                                                                <input type="text" class="form-control mb-2" name="detail[{{$key}}][name]" value="{{$value['name']}}" id="inlineFormInput" placeholder="Name">
                                                            </div>
                                                            <div class="col-auto">
                                                                <div class="input-group mb-2">
                                                                    <input type="text" class="form-control" name="detail[{{$key}}][value]" value="{{$value['value']}}" id="inlineFormInputGroup" placeholder="Value">
                                                                </div>
                                                            </div>
                                                            <div class="col-auto">
                                                                @if($key == 1)
                                                                <button type="button" class="btn btn-icon waves-effect waves-light btn-info mb-2 addNewInformation"> <i class="fa fa-plus-circle"></i> </button>
                                                                @else
                                                                <button type="button" class="btn btn-icon waves-effect waves-light btn-info mb-2 addNewInformation"> <i class="fa fa-plus-circle"></i> </button>
                                                                <button type="button" class="btn btn-icon waves-effect waves-light btn-danger mb-2 DeleteInformation"> <i class="fa fa-times-circle-o"></i> </button>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        @endif
                                                    @endforeach
                                                @else
                                                <div class="form-row align-items-center box_details">
                                                    <div class="col-auto">
                                                        <input type="text" class="form-control mb-2" name="detail[1][name]" id="inlineFormInput" placeholder="Name">
                                                    </div>
                                                    <div class="col-auto">
                                                        <div class="input-group mb-2">
                                                            <input type="text" class="form-control" name="detail[1][value]" id="inlineFormInputGroup" placeholder="Value">
                                                        </div>
                                                    </div>
                                                    <div class="col-auto">
                                                        <button type="button" class="btn btn-icon waves-effect waves-light btn-info mb-2 addNewInformation"> <i class="fa fa-plus-circle"></i> </button>
                                                    </div>
                                                </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            <label for="inputEmail4" class="col-form-label">Discription</label>
                                            <textarea class="discription" name="discription">{!! old('discription',isset($product->discription)?$product->discription:'')!!}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row justify-content-end">
                                <label class="col-2 col-form-label">Status</label>
                                <div class=" col-10">
                                    <div class="checkbox checkbox-primary">
                                        <input id="status" type="checkbox" name="status" {{old('status', isset($product->status)?$product->status:'0') && old('status', isset($product->status)?$product->status:'0') == '1'|| old('status', isset($product->status)?$product->status:'0') == 'on' ?'checked':''}}>
                                        <label for="status">
                                            <span class="status">{{old('status', isset($product->status)?$product->status:'0') && old('status', isset($product->status)?$product->status:'0') == '1'|| old('status', isset($product->status)?$product->status:'0') == 'on' ?'On':'Off'}}</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group mb-0 justify-content-end row">
                                <div class="col-9">
                                    <button type="submit" class="btn btn-info waves-effect waves-light">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
            <!-- end row -->

        </div> <!-- end card-box -->
    </div><!-- end col -->
</div>
@stop

@section('head')
<link href="{{ URL::asset('adminstyle/plugins/bootstrap-fileupload/bootstrap-fileupload.css')}}" rel="stylesheet" />

<script src="//cdn.ckeditor.com/4.9.1/standard/ckeditor.js"></script>
@stop
@section('script')
<script src="{{ URL::asset('adminstyle/plugins/bootstrap-fileupload/bootstrap-fileupload.js')}}"></script>
<script src="{{ URL::asset('adminstyle/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js')}}"></script>
<script src="{{ URL::asset('adminstyle/plugins/autoNumeric/autoNumeric.js')}}"></script>

<script>
    let i = $('.boxInformation .box_details').length
    $(document).on('click','.addNewInformation', function() {
        i++
        newImage =
        '<div class="form-row align-items-center box_details">'
        +'    <div class="col-auto">'
        +'        <input type="text" class="form-control mb-2" name="detail['+i+'][name]" id="inlineFormInput" placeholder="Name">'
        +'    </div>'
        +'    <div class="col-auto">'
        +'        <div class="input-group mb-2">'
        +'            <input type="text" class="form-control" name="detail['+i+'][value]" id="inlineFormInputGroup" placeholder="Value">'
        +'        </div>'
        +'    </div>'
        +'    <div class="col-auto">'
        +'        <button type="button" class="btn btn-icon waves-effect waves-light btn-info mb-2 addNewInformation"> <i class="fa fa-plus-circle"></i> </button>'
        +'        <button type="button" class="btn btn-icon waves-effect waves-light btn-danger mb-2 DeleteInformation"> <i class="fa fa-times-circle-o"></i> </button>'
        +'    </div>'
        +'</div>';
            $('.boxInformation').append(newImage);
    });
    $(document).on('click','.DeleteInformation',function() {
        $(this).parents('.box_details').remove();
    });

    $(document).on('click','.btn-delete-image-item',function() {
        $(this).parents('.ImageUploadItem').remove();
    });

    let y = $('.ImageUpload .ImageUploadItem').length
    $(document).on('click','.btn-plus-image', function() {
        y++;
        newImage ='<div class="fileupload fileupload-new ImageUploadItem col-auto" data-provides="fileupload">'
            +'    <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">'
            +'        <img src="https://upload.wikimedia.org/wikipedia/commons/6/6c/No_image_3x4.svg" alt="image">'
            +'    </div>'
            +'<div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>'
            +'    <div>'
            +'        <button type="button" class="btn btn-custom btn-file">'
            +'            <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Select image</span>'
            +'            <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>'
            +'            <input type="file" class="btn-light btnUploadImageItem" onchange="UploadImage(this);" accept="image/x-png,image/gif,image/jpeg">'
            +'            <input type="hidden" class="ValueImageUpload"  name="image['+y+'][name]">'
            +'        </button>'
            +'<button type="button" class="btn btn-info btn-plus-image"> <i class="fa fa-plus-circle"></i> </button>'
            +'        <button type="button" class="btn btn-danger btn-delete-image-item"> <i class="fa fa-times-circle-o"></i> </button>'
            +'    </div>'
            +'</div>';
        $('.ImageUpload').append(newImage);
    });

    CKEDITOR.replace( 'discription', {
        filebrowserBrowseUrl: '{{ asset('adminstyle/plugins/ckfinder/ckfinder.html') }}',
        filebrowserImageBrowseUrl: '{{ asset('adminstyle/plugins/ckfinder/ckfinder.html?type=Images') }}',
        filebrowserFlashBrowseUrl: '{{ asset('adminstyle/plugins/ckfinder/ckfinder.html?type=Flash') }}',
        filebrowserUploadUrl: '{{ asset('adminstyle/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') }}',
        filebrowserImageUploadUrl: '{{ asset('adminstyle/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images') }}',
        filebrowserFlashUploadUrl: '{{ asset('adminstyle/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash') }}'
    } );
    $('#status').change(function(){
        if(this.checked)
        {
            ($('.status')).html('On');
        }
        else
        {
            ($('.status')).html('Off');
        }
    });
    $(function() {
      $("#price, #price_sale").keyup(function() { // input on change
        var result = 100 - (parseInt($("#price_sale").val().replace(',','')) / parseInt($("#price").val().replace(',','')) * 100);
        $('#rate').html(result); //shows value in "#rate"
      })
    });

    function UploadImage(e){

        var form_data = new FormData();
            form_data.append('file', e.files[0]);
            form_data.append('_token', '{{csrf_token()}}');
        $.ajax({
          url: "{{route('admin.uploads.image')}}", // Url to which the request is send
          data: form_data,
          type: 'POST',
          contentType: false,
          processData: false,
          success: function(res)   // A function to be called if request succeeds
          {
            dataSuccess = JSON.parse(res);
            console.log(res);
            $('#image_show').attr('src', dataSuccess.filename);
            $('#images_show_data').val(dataSuccess.filename);
            $(e).next('input').val(dataSuccess.filename)
          }
        });
      };
     jQuery(function($) {
        $('.autonumber').autoNumeric('init');
    });
    jQuery.browser = {};
    (function () {
        jQuery.browser.msie = false;
        jQuery.browser.version = 0;
        if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
            jQuery.browser.msie = true;
            jQuery.browser.version = RegExp.$1;
        }
    })();

</script>
@stop
