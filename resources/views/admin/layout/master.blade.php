<!DOCTYPE html>
<html>
   <head>
        <meta charset="utf-8" />
        <title>{{isset($title)?$title:"Home"}} | Admin</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App favicon -->
        <link rel="shortcut icon" href="{{ URL::asset('adminstyle/assets/images/favicon.ico')}}">

        <!-- App css -->
        <link href="{{ URL::asset('adminstyle/assets2/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('adminstyle/assets2/css/icons.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('adminstyle/assets2/css/metismenu.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('adminstyle/assets2/css/style.css')}}" rel="stylesheet" type="text/css" />

        <script src="{{ URL::asset('adminstyle/assets2/js/modernizr.min.js')}}"></script>
        @yield('head')

    </head>


    <body>

        <!-- Begin page -->
        <div id="wrapper">

            <!-- ========== Left Sidebar Start ========== -->
            @include('admin.layout.include.sizebardefault')
            <!-- Left Sidebar End -->



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->

            <div class="content-page">

                <!-- Top Bar Start -->
                @include('admin.layout.include.topdefault')
                <!-- Top Bar End -->



                <!-- Start Page content -->
                <div class="content">
                    <div class="container-fluid">
                        @yield('content')
                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer text-right">
                    2018 © Admin. - Alvin
                </footer>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->



        <!-- END wrapper -->
        <!-- jQuery  -->
        <script src="{{ URL::asset('adminstyle/assets2/js/jquery.min.js')}}"></script>
        <script src="{{ URL::asset('adminstyle/assets2/js/popper.min.js')}}"></script>
        <script src="{{ URL::asset('adminstyle/assets2/js/bootstrap.min.js')}}"></script>
        <script src="{{ URL::asset('adminstyle/assets2//js/metisMenu.min.js')}}"></script>
        <script src="{{ URL::asset('adminstyle/assets2/js/waves.js')}}"></script>
        <script src="{{ URL::asset('adminstyle/assets2/js/jquery.slimscroll.js')}}"></script>

        <script src="{{ URL::asset('adminstyle/plugins/jquery-knob/jquery.knob.js')}}"></script>

        @yield('script')
        <!-- App js -->
        <script src="{{ URL::asset('adminstyle/assets2/js/jquery.core.js')}}"></script>
        <script src="{{ URL::asset('adminstyle/assets2/js/jquery.app.js')}}"></script>

    </body>
</html>
