<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>{{isset($title)?$title:"Home"}} | Admin</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App favicon -->
        <link rel="shortcut icon" href="{{ URL::asset('adminstyle/assets/images/favicon.ico')}}">

        <!-- App css -->
        <link href="{{ URL::asset('adminstyle/assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('adminstyle/assets/css/icons.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('adminstyle/assets/css/style.css')}}" rel="stylesheet" type="text/css" />

        <script src="{{ URL::asset('adminstyle/assets/js/modernizr.min.js')}}"></script>
        @yield('head')

    </head>

    <body>

        <!-- Navigation Bar-->
        @include('admin.layout.include.top')
        <!-- End Navigation Bar-->


        <div class="wrapper">
            <div class="container-fluid">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <div class="btn-group pull-right">
                                <ol class="breadcrumb hide-phone p-0 m-0">
                                @isset($breadcrumb))
                                    @foreach($breadcrumb as $breadcrumbItem)
                                    <li class="breadcrumb-item">{{$breadcrumbItem['name']}}</a>
                                    @endforeach
                                @endisset
                                </ol>
                            </div>
                            <h4 class="page-title">{{isset($title)?$title:'Empty Title'}}</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->

                @yield('content')


            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->


        <!-- Footer -->
        <footer class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        2018 © Highdmin. - Coderthemes.com
                    </div>
                </div>
            </div>
        </footer>
        <!-- End Footer -->

        @yield('modal')

        <!-- jQuery  -->
        <script src="{{ URL::asset('adminstyle/assets/js/jquery.min.js')}}"></script>
        <script src="{{ URL::asset('adminstyle/assets/js/popper.min.js')}}"></script>
        <script src="{{ URL::asset('adminstyle/assets/js/bootstrap.min.js')}}"></script>
        <script src="{{ URL::asset('adminstyle/assets/js/waves.js')}}"></script>
        <script src="{{ URL::asset('adminstyle/assets/js/jquery.slimscroll.js')}}"></script>

        <!--[if IE]>
        <script type="text/javascript" src="../plugins/jquery-knob/excanvas.js"></script>
        <![endif]-->
        <script src="{{ URL::asset('adminstyle/plugins/jquery-knob/jquery.knob.js')}}"></script>

        <!-- App js -->
        <script src="{{ URL::asset('adminstyle/assets/js/jquery.core.js')}}"></script>
        <script src="{{ URL::asset('adminstyle/assets/js/jquery.app.js')}}"></script>
        @yield('script')


    </body>
</html>
