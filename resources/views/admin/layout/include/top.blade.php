<header id="topnav">
    <div class="topbar-main">
        <div class="container-fluid">

            <!-- Logo container-->
            <div class="logo">
                <!-- Text Logo -->
                <!-- <a href="index.html" class="logo">
                    <span class="logo-small"><i class="mdi mdi-radar"></i></span>
                    <span class="logo-large"><i class="mdi mdi-radar"></i> Highdmin</span>
                </a> -->
                <!-- Image Logo -->
                <a href="index.html" class="logo">
                    <img src="{{ URL::asset('adminstyle/assets/images/logo_sm.png')}}" alt="" height="26" class="logo-small">
                    <img src="{{ URL::asset('adminstyle/assets/images/logo.png')}}" alt="" height="22" class="logo-large">
                </a>

            </div>
            <!-- End Logo container-->


            <div class="menu-extras topbar-custom">

                <ul class="list-unstyled topbar-right-menu float-right mb-0">

                    <li class="menu-item">
                        <!-- Mobile menu toggle-->
                        <a class="navbar-toggle nav-link">
                            <div class="lines">
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                        </a>
                        <!-- End mobile menu toggle-->
                    </li>

                    <li class="dropdown notification-list">
                        <a class="nav-link dropdown-toggle waves-effect nav-user" data-toggle="dropdown" href="#" role="button"
                           aria-haspopup="false" aria-expanded="false">
                            <img src="{{ URL::asset('adminstyle/assets/images/users/avatar-1.jpg')}}" alt="user" class="rounded-circle"> <span class="ml-1 pro-user-name">{{Auth::check()?Auth::user()->name:"Login"}} <i class="mdi mdi-chevron-down"></i> </span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                            <!-- item-->
                  <!--           <div class="dropdown-item noti-title">
                                <h6 class="text-overflow m-0">Welcome !</h6>
                            </div>
 -->
                            <!-- item-->
                            <a href="javascript:void(0);" class="dropdown-item notify-item">
                                <i class="fi-head"></i> <span>My Account</span>
                            </a>

                            <!-- item-->
                     <!--        <a href="javascript:void(0);" class="dropdown-item notify-item">
                                <i class="fi-cog"></i> <span>Settings</span>
                            </a>
 -->
                            <!-- item-->
<!--                             <a href="javascript:void(0);" class="dropdown-item notify-item">
                                <i class="fi-help"></i> <span>Support</span>
                            </a>
 -->
                            <!-- item-->
<!--                             <a href="javascript:void(0);" class="dropdown-item notify-item">
                                <i class="fi-lock"></i> <span>Lock Screen</span>
                            </a>
 -->
                            <!-- item-->
                            <a href="{{route('admin.getLogout')}}" class="dropdown-item notify-item">
                                <i class="fi-power"></i> <span>Logout</span>
                            </a>

                        </div>
                    </li>
                </ul>
            </div>
            <!-- end menu-extras -->

            <div class="clearfix"></div>

        </div> <!-- end container -->
    </div>
    <!-- end topbar-main -->

    <div class="navbar-custom">
        <div class="container-fluid">
            <div id="navigation">
                <!-- Navigation Menu-->
                <ul class="navigation-menu">

                    <li class="has-submenu">
                        <a href="{{route('admin.dashboard')}}"><i class="icon-speedometer"></i>Dashboard</a>
                    </li>

                    <li class="has-submenu">
                        <a href="{{route('admin.products.product.list')}}"><i class="icon-present"></i>Products</a>
                        <ul class="submenu">
                            <li class="has-submenu">
                                <a href="{{route('admin.products.category.list')}}">Category</a>
                                <ul class="submenu">
                                    <li><a href="{{route('admin.products.category.list')}}">List</a></li>
                                    <li><a href="{{route('admin.products.category.store')}}">Create</a></li>
                                </ul>
                            </li>
                            <li class="has-submenu">
                                <a href="{{route('admin.products.product.list')}}">Product</a>
                                <ul class="submenu">
                                    <li><a href="{{route('admin.products.product.list')}}">List</a></li>
                                    <li><a href="{{route('admin.products.product.store')}}">Create</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>

                    <li class="has-submenu">
                        <a href="{{route('admin.banner.list')}}"><i class="icon-screen-desktop"></i>Banner</a>
                        <ul class="submenu">
                            <li><a href="{{route('admin.banner.list')}}">List</a></li>
                            <li><a href="{{route('admin.banner.store')}}">Create</a></li>
                        </ul>
                    </li>


                    <li class="has-submenu">
                        <a href="{{route('admin.contact.list')}}"><i class="icon-bubbles"></i>Contact</a>
                    </li>
                    <li class="has-submenu">
                        <a href="{{route('admin.about.store')}}"><i class="icon-bubbles"></i>About</a>
                    </li>

                     <li class="has-submenu">
                        <a href="{{route('admin.user.list')}}"><i class="icon-user"></i>User</a>
                        <ul class="submenu">
                            <li class="has-submenu">
                                <a href="{{route('admin.permission.list')}}">Permissions</a>
                                <ul class="submenu">
                                    <li><a href="{{route('admin.permission.list')}}">List</a></li>
                                    <li><a href="{{route('admin.permission.store')}}">Create</a></li>
                                </ul>
                            </li>
                            <li class="has-submenu">
                                <a href="{{route('admin.role.list')}}">Role</a>
                                <ul class="submenu">
                                    <li><a href="{{route('admin.role.list')}}">List</a></li>
                                    <li><a href="{{route('admin.role.store')}}">Create</a></li>
                                </ul>
                            </li>
                            <li>

                            </li>
                        </ul>
                    </li>

                </ul>
            </div>
        </div>
    </div>
</header>
