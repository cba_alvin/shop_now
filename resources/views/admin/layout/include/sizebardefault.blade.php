<div class="left side-menu">

    <div class="slimscroll-menu" id="remove-scroll">

        <!-- LOGO -->
        <div class="topbar-left">
            <a href="index.html" class="logo">
                <span>
                    <img src="{{ URL::asset('adminstyle/assets2/images/logo.png')}}" alt="" height="22">
                </span>
                <i>
                    <img src="{{ URL::asset('adminstyle/assets2/images/logo_sm.png')}}" alt="" height="28">
                </i>
            </a>
        </div>

        <!-- User box -->
        <div class="user-box">
            <div class="user-img">
                <img src="{{ URL::asset('adminstyle/assets2/images/users/avatar-1.jpg')}}" alt="user-img" title="Mat Helme" class="rounded-circle img-fluid">
            </div>
            <h5><a href="#">Maxine Kennedy</a> </h5>
            <p class="text-muted">Admin Head</p>
        </div>

        <!--- Sidemenu -->
        <div id="sidebar-menu">

            <ul class="metismenu" id="side-menu">

                <!--<li class="menu-title">Navigation</li>-->

                <li>
                    <a href="{{route('admin.dashboard')}}">
                        <i class="fi-air-play"></i> <span> Dashboard </span>
                    </a>
                </li>

                <li>
                    <a href="javascript: void(0);"><i class="fi-layers"></i> <span> Product </span> <span class="menu-arrow"></span></a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li><a href="{{route('admin.products.category.list')}}">Category</a></li>
                        <li><a href="{{route('admin.products.product.list')}}">Product</a></li>
                    </ul>
                </li>

                <li>
                    <a href="{{route('admin.banner.list')}}">
                        <i class="fi-image"></i> <span> Banner </span>
                    </a>
                </li>

                <li>
                    <a href="{{route('admin.contact.list')}}">
                        <i class="fi-mail"></i> <span> Contact </span>
                    </a>
                </li>

                <li>
                    <a href="{{route('admin.about.store')}}">
                        <i class="fi-eye"></i> <span> About </span>
                    </a>
                </li>

                <li>
                    <a href="javascript: void(0);"><i class="fi-head"></i> <span> User </span> <span class="menu-arrow"></span></a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li><a href="{{route('admin.user.list')}}">User</a></li>
                        <li><a href="{{route('admin.role.list')}}">Role</a></li>
                        <li><a href="{{route('admin.permission.list')}}">Permission</a></li>
                    </ul>
                </li>

            </ul>

        </div>
        <!-- Sidebar -->

        <div class="clearfix"></div>

    </div>
    <!-- Sidebar -left -->

</div>
