@extends('admin.layout.master',['title'=>isset($id)?"Update permission ".$role->name:'Create new permission'])

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card-box">
            <h4 class="m-t-0 header-title">role {{isset($id)?"Update":'Create'}}</h4>

            <div class="row">
                <div class="col-12">
                    <div class="p-20">
                        <form class="form-horizontal" action="{{isset($id)?route('admin.role.store',['id'=>$id]):route('admin.role.store')}}" role="form" method="POST">
                            {{ csrf_field() }}
                            <div class="form-group row">
                                <label class="col-2 col-form-label">Name<span class="text-danger">*</span></label>
                                <div class="col-10">
                                    <input type="text" class="form-control" name="name" value="{{ old('name', isset($role->name)?$role->name:'') }}" placeholder="Name role">
                                    @if($errors->has('name'))
                                    <ul class="parsley-errors-list filled" id="parsley-id-18"><li class="parsley-required">{{$errors->first('name')}}</li></ul>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-2 col-form-label">Permissions<span class="text-danger">*</span></label>
                                <div class="col-6 form-row align-items-center">
                                    @if($permission)
                                    @php
                                    $key = 0;
                                    @endphp
                                        @foreach($permission as $item)
                                        <div class="col-auto">
                                            <div class="form-check mb-3">
                                                <input class="form-check-input" name="permissions[]" type="checkbox" value="{{$item->name}}" id="autoSizingCheck{{$key}}" @if(in_array($item->name,old('permissions',isset($role->permissions)?$role->permissions->pluck('name')->toArray():[]))) {{"checked"}} @endif>
                                                <label class="form-check-label" for="autoSizingCheck{{$key}}">
                                                    {{$item->name}}
                                                </label>
                                            </div>
                                        </div>
                                        @php
                                        $key++;
                                        @endphp
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                            <div class="form-group mb-0 justify-content-end row">
                                <div class="col-9">
                                    <button type="submit" class="btn btn-info waves-effect waves-light">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
            <!-- end row -->

        </div> <!-- end card-box -->
    </div><!-- end col -->
</div>
@stop
