@extends('admin.layout.master',['title'=>isset($id)?"Update User ".$user->name:'Create new User'])

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card-box">
            <h4 class="m-t-0 header-title">{{isset($id)?"Update":'Create new'}} User</h4>

            <div class="row">
                <div class="col-12">
                    <div class="p-20">
                        <form class="form-horizontal" action="{{isset($id)?route('admin.user.store',['id'=>$id]):route('admin.user.store')}}" role="form" method="POST">
                            {{ csrf_field() }}
                            <div class="form-group row">
                                <label class="col-2 col-form-label">Email</label>
                                <div class="col-10">
                                    <input type="text" class="form-control" name="email" value="{{ old('email', isset($user->email)?$user->email:'') }}" placeholder="email">
                                    @if($errors->has('email'))
                                    <ul class="parsley-errors-list filled" id="parsley-id-18"><li class="parsley-required">{{$errors->first('email')}}</li></ul>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-2 col-form-label">Passworld</label>
                                <div class="col-10">
                                    <input type="password" class="form-control" name="password" value="" placeholder="password">
                                    @if($errors->has('password'))
                                    <ul class="parsley-errors-list filled" id="parsley-id-18"><li class="parsley-required">{{$errors->first('password')}}</li></ul>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-2 col-form-label">Name</label>
                                <div class="col-10">
                                    <input type="text" class="form-control" name="name" value="{{ old('name', isset($user->name)?$user->name:'') }}" placeholder="name">
                                    @if($errors->has('name'))
                                    <ul class="parsley-errors-list filled" id="parsley-id-18"><li class="parsley-required">{{$errors->first('title')}}</li></ul>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-2 col-form-label">Avatar</label>
                                <div class="col-10">
                                    <div class="fileupload fileupload-new ImageUploadItem" data-provides="fileupload">
                                        <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                            <img src="{{ URL::asset(old('avatar', isset($user->avatar)?$user->avatar:'No_image_3x4.svg'))}}" alt="image" >
                                        </div>
                                        <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                        <div>
                                            <button type="button" class="btn btn-custom btn-file">
                                                <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Select image</span>
                                                <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                                                <input type="file" class="btn-light btnUploadImageItem" onchange="UploadImage(this);" accept="image/x-png,image/gif,image/jpeg">
                                                <input type="hidden" class="ValueImageUpload" value="{{ old('avatar', isset($user->avatar)?$user->avatar:'')}}"  name="avatar">
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-2 col-form-label">Address</label>
                                <div class="col-10">
                                    <input type="text" class="form-control" name="address" value="{{ old('address', isset($user->address)?$user->address:'') }}" placeholder="address">
                                    @if($errors->has('address'))
                                    <ul class="parsley-errors-list filled" id="parsley-id-18"><li class="parsley-required">{{$errors->first('address')}}</li></ul>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-2 col-form-label">Phone</label>
                                <div class="col-10">
                                    <input type="text" class="form-control" name="phone" value="{{ old('phone', isset($user->phone)?$user->phone:'') }}" placeholder="phone">
                                    @if($errors->has('phone'))
                                    <ul class="parsley-errors-list filled" id="parsley-id-18"><li class="parsley-required">{{$errors->first('phone')}}</li></ul>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-2 col-form-label">Status</label>
                                <div class="col-10">
                                    <select class="selectpicker m-b-0" name="status" data-style="btn-light">
                                        <option data-icon="mdi mdi-account-multiple" value="1" {{old('status',isset($user->status)?$user->status:null) == 1?"selected":""}}>User</option>
                                        <option data-icon="mdi mdi mdi-account-key" value="2" {{old('status',isset($user->status)?$user->status:null) == 2?"selected":""}}>Admin</option>
                                        <option data-icon="mdi mdi-account-off" value="3" {{old('status',isset($user->status)?$user->status:null) == 3?"selected":""}}>Banned</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-2 col-form-label">Role</label>
                                <div class="col-10">
                                    @if($roles)
                                    @php
                                    $key = 0;
                                    @endphp
                                    @foreach($roles as $item)
                                    <div class="col-auto">
                                        <div class="form-check mb-3">
                                            <input class="form-check-input" name="roles[]" type="checkbox" value="{{$item->name}}" id="autoSizingCheck{{$key}}" @if(in_array($item->name,old('roles',isset($user->roles)?$user->roles->pluck('name')->toArray():[]))) {{"checked"}} @endif>
                                            <label class="form-check-label" for="autoSizingCheck{{$key}}">
                                                {{$item->name}}
                                            </label>
                                        </div>
                                    </div>
                                    @php
                                    $key++;
                                    @endphp
                                    @endforeach
                                    @endif
                                </div>
                            </div>

                            <div class="form-group mb-0 justify-content-end row">
                                <div class="col-9">
                                    <button type="submit" class="btn btn-info waves-effect waves-light">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
            <!-- end row -->

        </div> <!-- end card-box -->
    </div><!-- end col -->
</div>
@stop

@section('head')
<link href="{{ URL::asset('adminstyle/plugins/bootstrap-fileupload/bootstrap-fileupload.css')}}" rel="stylesheet" />
<link href="{{ URL::asset('adminstyle/plugins/bootstrap-tagsinput/css/bootstrap-tagsinput.css')}}" rel="stylesheet" />
<link href="{{ URL::asset('adminstyle/plugins/bootstrap-select/css/bootstrap-select.min.css')}}" rel="stylesheet" />
<link href="{{ URL::asset('adminstyle/plugins/select2/css/select2.min.css')}}" rel="stylesheet" />
<link href="{{ URL::asset('adminstyle/plugins/switchery/switchery.min.css')}}" rel="stylesheet" />

@stop
@section('script')
<script src="{{ URL::asset('adminstyle/plugins/bootstrap-fileupload/bootstrap-fileupload.js')}}"></script>
<script src="{{ URL::asset('adminstyle/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js')}}"></script>
<script src="{{ URL::asset('adminstyle/plugins/autoNumeric/autoNumeric.js')}}"></script>
<script src="{{ URL::asset('adminstyle/plugins/switchery/switchery.min.js')}}"></script>
<script src="{{ URL::asset('adminstyle/plugins/bootstrap-tagsinput/js/bootstrap-tagsinput.min.js')}}"></script>
<script src="{{ URL::asset('adminstyle/plugins/select2/js/select2.min.js')}}"></script>
<script src="{{ URL::asset('adminstyle/plugins/bootstrap-select/js/bootstrap-select.js')}}"></script>
<script src="{{ URL::asset('adminstyle/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js')}}"></script>
<script src="{{ URL::asset('adminstyle/plugins/bootstrap-maxlength/bootstrap-maxlength.js')}}"></script>

<script>
function UploadImage(e){
    var form_data = new FormData();
        form_data.append('file', e.files[0]);
        form_data.append('_token', '{{csrf_token()}}');
    $.ajax({
      url: "{{route('admin.uploads.image',['type'=>'users'])}}", // Url to which the request is send
      data: form_data,
      type: 'POST',
      contentType: false,
      processData: false,
      success: function(res)   // A function to be called if request succeeds
      {
        dataSuccess = JSON.parse(res);
        console.log(res);
        $('#image_show').attr('src', dataSuccess.filename);
        $('#images_show_data').val(dataSuccess.filename);
        $(e).next('input').val(dataSuccess.filename)
      }
    });
  };
</script>
@stop
