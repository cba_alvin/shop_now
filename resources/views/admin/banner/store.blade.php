@extends('admin.layout.master',['title'=>isset($id)?"Update category ".$banner->name:'Create new category'])

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card-box">
            <h4 class="m-t-0 header-title">Category {{isset($id)?"Update":'Create'}}</h4>

            <div class="row">
                <div class="col-12">
                    <div class="p-20">
                        <form class="form-horizontal" action="{{isset($id)?route('admin.banner.store',['id'=>$id]):route('admin.banner.store')}}" role="form" method="POST">
                            {{ csrf_field() }}
                            <div class="form-group row">
                                <label class="col-2 col-form-label">Title<span class="text-danger">*</span></label>
                                <div class="col-10">
                                    <input type="text" class="form-control" name="title" value="{{ old('title', isset($banner->title)?$banner->title:'') }}" placeholder="title">
                                    @if($errors->has('title'))
                                    <ul class="parsley-errors-list filled" id="parsley-id-18"><li class="parsley-required">{{$errors->first('title')}}</li></ul>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-2 col-form-label">Url<span class="text-danger">*</span></label>
                                <div class="col-10">
                                    <input type="text" class="form-control" name="url" value="{{ old('url', isset($banner->url)?$banner->url:'') }}" placeholder="url">
                                    @if($errors->has('url'))
                                    <ul class="parsley-errors-list filled" id="parsley-id-18"><li class="parsley-required">{{$errors->first('url')}}</li></ul>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-2 col-form-label">Image<span class="text-danger">*</span></label>
                                <div class="col-10">
                                    <div class="fileupload fileupload-new ImageUploadItem" data-provides="fileupload">
                                        <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                            <img src="{{ URL::asset(old('image', isset($banner->image)?$banner->image:'No_image_3x4.svg'))}}" alt="image" >
                                        </div>
                                        <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                        <div>
                                            <button type="button" class="btn btn-custom btn-file">
                                                <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Select image</span>
                                                <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                                                <input type="file" class="btn-light btnUploadImageItem" onchange="UploadImage(this);" accept="image/x-png,image/gif,image/jpeg">
                                                <input type="hidden" class="ValueImageUpload" value="{{ old('image', isset($banner->image)?$banner->image:'')}}"  name="image">
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                             <div class="form-group row">
                                <label class="col-2 col-form-label">Content</label>
                                <div class="col-10">
                                    <textarea class="form-control" name="content" rows="5">{{ old('content', isset($banner->content)?$banner->content:'')}}</textarea>
                                </div>
                            </div>
                            <div class="form-group mb-0 justify-content-end row">
                                <div class="col-9">
                                    <button type="submit" class="btn btn-info waves-effect waves-light">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
            <!-- end row -->

        </div> <!-- end card-box -->
    </div><!-- end col -->
</div>
@stop

@section('head')
<link href="{{ URL::asset('adminstyle/plugins/bootstrap-fileupload/bootstrap-fileupload.css')}}" rel="stylesheet" />
@stop
@section('script')
<script src="{{ URL::asset('adminstyle/plugins/bootstrap-fileupload/bootstrap-fileupload.js')}}"></script>
<script src="{{ URL::asset('adminstyle/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js')}}"></script>
<script src="{{ URL::asset('adminstyle/plugins/autoNumeric/autoNumeric.js')}}"></script>
<script>
function UploadImage(e){
    var form_data = new FormData();
        form_data.append('file', e.files[0]);
        form_data.append('_token', '{{csrf_token()}}');
    $.ajax({
      url: "{{route('admin.uploads.image')}}", // Url to which the request is send
      data: form_data,
      type: 'POST',
      contentType: false,
      processData: false,
      success: function(res)   // A function to be called if request succeeds
      {
        dataSuccess = JSON.parse(res);
        console.log(res);
        $('#image_show').attr('src', dataSuccess.filename);
        $('#images_show_data').val(dataSuccess.filename);
        $(e).next('input').val(dataSuccess.filename)
      }
    });
  };
</script>
@stop
