@extends('admin.layout.master',['title'=>'Dashboard'])
@section('content')
<div class="row">
    <div class="col-12">
        <div class="card-box">
            <h4 class="header-title mb-4">Overview</h4>

            <div class="text-center mt-4 mb-4">
                <div class="row">
                    <div class="col-xs-6 col-sm-3">
                        <div class="card-box widget-flat border-custom bg-custom text-white">
                            <i class="fi-align-justify"></i>
                            <h3 class="m-b-10">{{ $cateogryProduct->where('status','1')->count() }}</h3>
                            <p class="text-uppercase m-b-5 font-13 font-600">Category</p>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-3">
                        <div class="card-box bg-primary widget-flat border-primary text-white">
                            <i class="fi-layers"></i>
                            <h3 class="m-b-10"><h3 class="">{{$product->count()}}</h3></h3>
                            <p class="text-uppercase m-b-5 font-13 font-600">Product</p>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-3">
                        <div class="card-box widget-flat border-primary bg-danger text-white">
                            <i class="fi-mail"></i>
                            <h3 class="m-b-10">{{$contact->count()}}</h3>
                            <p class="text-uppercase m-b-5 font-13 font-600">Contact</p>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-3">
                        <div class="card-box bg-success widget-flat border-primary text-white">
                            <i class="fi-image"></i>
                            <h3 class="m-b-10">{{$banner->count()}}</h3>
                            <p class="text-uppercase m-b-5 font-13 font-600">Banner</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end row -->

<div class="row">
    <div class="col-lg-4">
        <div class="card-box">
            <h4 class="header-title mb-4">Contact</h4>

            <div class="comment-list slimscroll" style="max-height: 370px;">
                @if($contact->where('views',0)->limit(10)->get())
                @foreach($contact->where('views',0)->get() as $itemcontact)
               <a href="{{route('admin.contact.detail',['id'=>$itemcontact->id])}}">
                    <div class="comment-box-item">
                        <div class="badge badge-pill badge-success">Contact new</div>
                        <p class="commnet-item-date">{{$itemcontact->updated_at?$itemcontact->updated_at->diffForHumans():''}}</p>
                        <h6 class="commnet-item-msg">{!! nl2br($itemcontact->content)!!}</h6>
                        <p class="commnet-item-user">{{$itemcontact->name}}</p>
                    </div>
                </a>
                @endforeach
                @else
                    no contact!
                @endif
            </div>

        </div>
    </div>
    <div class="col-lg-4">
        <div class="card-box">
            <h4 class="header-title mb-4">Latest Comments</h4>

            <div class="comment-list slimscroll" style="max-height: 370px;">
                <a href="#">
                    <div class="comment-box-item">
                        <div class="badge badge-pill badge-success">Ubold - Admin</div>
                        <p class="commnet-item-date">1 month ago</p>
                        <h6 class="commnet-item-msg">Do you have any plans to add a vertical menu?</h6>
                        <p class="commnet-item-user">Ubold User</p>
                    </div>
                </a>
                <a href="#">
                    <div class="comment-box-item">
                        <div class="badge badge-pill badge-warning">Adminox - Admin</div>
                        <p class="commnet-item-date">1 month ago</p>
                        <h6 class="commnet-item-msg">Hello, what is your plan to upgrade Bootstrap 4 versions? Beta or wait for stable?</h6>
                        <p class="commnet-item-user">Ubold User</p>
                    </div>
                </a>
                <a href="#">
                    <div class="comment-box-item">
                        <div class="badge badge-pill badge-primary">Staro - Landing</div>
                        <p class="commnet-item-date">1 month ago</p>
                        <h6 class="commnet-item-msg">Hi coderthemes – do you have PSD for this admin UI? I could really use it.</h6>
                        <p class="commnet-item-user">Ubold User</p>
                    </div>
                </a>
                <a href="#">
                    <div class="comment-box-item">
                        <div class="badge badge-pill badge-dark">Ubold - Admin</div>
                        <p class="commnet-item-date">1 month ago</p>
                        <h6 class="commnet-item-msg">Do you have any plans to add a vertical menu?</h6>
                        <p class="commnet-item-user">Ubold User</p>
                    </div>
                </a>
            </div>

        </div>
    </div>

    <div class="col-lg-4">
        <div class="card-box">
            <h4 class="header-title mb-4">Last Transactions</h4>

            <ul class="list-unstyled transaction-list slimscroll mb-0" style="max-height: 370px;">
                <li>
                    <i class="dripicons-arrow-down text-success"></i>
                    <span class="tran-text">Advertising</span>
                    <span class="pull-right text-success tran-price">+$230</span>
                    <span class="pull-right text-muted">07/09/2017</span>
                    <span class="clearfix"></span>
                </li>

                <li>
                    <i class="dripicons-arrow-up text-danger"></i>
                    <span class="tran-text">Support licence</span>
                    <span class="pull-right text-danger tran-price">-$965</span>
                    <span class="pull-right text-muted">07/09/2017</span>
                    <span class="clearfix"></span>
                </li>

                <li>
                    <i class="dripicons-arrow-down text-success"></i>
                    <span class="tran-text">Extended licence</span>
                    <span class="pull-right text-success tran-price">+$830</span>
                    <span class="pull-right text-muted">07/09/2017</span>
                    <span class="clearfix"></span>
                </li>

                <li>
                    <i class="dripicons-arrow-down text-success"></i>
                    <span class="tran-text">Advertising</span>
                    <span class="pull-right text-success tran-price">+$230</span>
                    <span class="pull-right text-muted">05/09/2017</span>
                    <span class="clearfix"></span>
                </li>

                <li>
                    <i class="dripicons-arrow-up text-danger"></i>
                    <span class="tran-text">New plugins added</span>
                    <span class="pull-right text-danger tran-price">-$452</span>
                    <span class="pull-right text-muted">05/09/2017</span>
                    <span class="clearfix"></span>
                </li>

                <li>
                    <i class="dripicons-arrow-down text-success"></i>
                    <span class="tran-text">Google Inc.</span>
                    <span class="pull-right text-success tran-price">+$230</span>
                    <span class="pull-right text-muted">04/09/2017</span>
                    <span class="clearfix"></span>
                </li>

                <li>
                    <i class="dripicons-arrow-up text-danger"></i>
                    <span class="tran-text">Facebook Ad</span>
                    <span class="pull-right text-danger tran-price">-$364</span>
                    <span class="pull-right text-muted">03/09/2017</span>
                    <span class="clearfix"></span>
                </li>

                <li>
                    <i class="dripicons-arrow-down text-success"></i>
                    <span class="tran-text">New sale</span>
                    <span class="pull-right text-success tran-price">+$230</span>
                    <span class="pull-right text-muted">03/09/2017</span>
                    <span class="clearfix"></span>
                </li>

                <li>
                    <i class="dripicons-arrow-down text-success"></i>
                    <span class="tran-text">Advertising</span>
                    <span class="pull-right text-success tran-price">+$230</span>
                    <span class="pull-right text-muted">29/08/2017</span>
                    <span class="clearfix"></span>
                </li>

                <li>
                    <i class="dripicons-arrow-up text-danger"></i>
                    <span class="tran-text">Support licence</span>
                    <span class="pull-right text-danger tran-price">-$854</span>
                    <span class="pull-right text-muted">27/08/2017</span>
                    <span class="clearfix"></span>
                </li>
            </ul>

        </div>
    </div>

</div>
@endsection
@section('script')
<script src="{{ URL::asset('adminstyle/plugins/jquery-ui/jquery-ui.min.js')}}"></script>

<script>
    $(document).ready(function () {

        $("#upcoming, #inprogress, #completed").sortable({
            connectWith: ".taskList",
            placeholder: 'task-placeholder',
            forcePlaceholderSize: true,
            update: function (event, ui) {

                var todo = $("#todo").sortable("toArray");
                var inprogress = $("#inprogress").sortable("toArray");
                var completed = $("#completed").sortable("toArray");
            }
        }).disableSelection();

    });
</script>
@stop
