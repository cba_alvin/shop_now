@extends('admin.layout.master',['title'=>isset($id)?"Update permission ".$permission->name:'Create new permission'])

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card-box">
            <h4 class="m-t-0 header-title">permission {{isset($id)?"Update":'Create'}}</h4>

            <div class="row">
                <div class="col-12">
                    <div class="p-20">
                        <form class="form-horizontal" action="{{isset($id)?route('admin.permission.store',['id'=>$id]):route('admin.permission.store')}}" role="form" method="POST">
                            {{ csrf_field() }}
                            <div class="form-group row">
                                <label class="col-2 col-form-label">Name<span class="text-danger">*</span></label>
                                <div class="col-10">
                                    <input type="text" class="form-control" name="name" value="{{ old('name', isset($permission->name)?$permission->name:'') }}" placeholder="Name permission">
                                    @if($errors->has('name'))
                                    <ul class="parsley-errors-list filled" id="parsley-id-18"><li class="parsley-required">{{$errors->first('name')}}</li></ul>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group mb-0 justify-content-end row">
                                <div class="col-9">
                                    <button type="submit" class="btn btn-info waves-effect waves-light">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
            <!-- end row -->

        </div> <!-- end card-box -->
    </div><!-- end col -->
</div>
@stop
