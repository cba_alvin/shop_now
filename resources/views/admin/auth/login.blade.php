<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Login | Admin</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App favicon -->
        <link rel="shortcut icon" href="{{ URL::asset('adminstyle/assets/images/favicon.ico')}}">
        <link href="{{ URL::asset('adminstyle/plugins/jquery-toastr/jquery.toast.min.css')}}" rel="stylesheet" />

        <!-- App css -->
        <link href="{{ URL::asset('adminstyle/assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('adminstyle/assets/css/icons.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('adminstyle/assets/css/style.css')}}" rel="stylesheet" type="text/css" />

        <script src="{{ URL::asset('adminstyle/assets/js/modernizr.min.js')}}"></script>

    </head>

    <body>

        <!-- Begin page -->
        <div class="accountbg" style="background: url('{{ URL::asset('adminstyle/assets/images/bg-1.jpg')}}');background-size: cover;"></div>

        <div class="wrapper-page account-page-full">

            <div class="card">
                <div class="card-block">

                    <div class="account-box">

                        <div class="card-box p-5">
                            <h2 class="text-uppercase text-center pb-4">
                                <a href="index.html" class="text-success">
                                    <span><img src="{{ URL::asset('adminstyle/assets/images/logo.png')}}" alt="" height="26"></span>
                                </a>
                            </h2>

                            <form class="" action="{{route('admin.postlogin')}}" method="POST">
                            {{ csrf_field() }}
                                <div class="form-group m-b-20 row">
                                    <div class="col-12">
                                        <label for="emailaddress">Email address</label>
                                        <input class="form-control" type="email" id="emailaddress" name="email" value="{{old('email')}}" required="" placeholder="Enter your email" autofocus>
                                    </div>
                                </div>

                                <div class="form-group row m-b-20">
                                    <div class="col-12">
                                        <!-- <a href="page-recoverpw.html" class="text-muted pull-right"><small>Forgot your password?</small></a> -->
                                        <label for="password">Password</label>
                                        <input class="form-control" type="password" required="" name="password" id="password" placeholder="Enter your password">
                                    </div>
                                </div>

                                <div class="form-group row m-b-20">
                                    <div class="col-12">

                                        <div class="checkbox checkbox-custom">
                                            <input id="remember" type="checkbox" name="remember">
                                            <label for="remember">
                                                Remember me
                                            </label>
                                        </div>

                                    </div>
                                </div>

                                <div class="form-group row text-center m-t-10">
                                    <div class="col-12">
                                        <button class="btn btn-block btn-custom waves-effect waves-light" type="submit">Sign In</button>
                                    </div>
                                </div>

                            </form>

<!--                             <div class="row m-t-50">
                                <div class="col-sm-12 text-center">
                                    <p class="text-muted">Don't have an account? <a href="page-register.html" class="text-dark m-l-5"><b>Sign Up</b></a></p>
                                </div>
                            </div>
 -->
                        </div>
                    </div>

                </div>
            </div>

            <div class="m-t-40 text-center">
                <p class="account-copyright">2018 © Admin. - Alvin</p>
            </div>

        </div>


        <!-- jQuery  -->
        <script src="{{ URL::asset('adminstyle/assets/js/jquery.min.js')}}"></script>
        <script src="{{ URL::asset('adminstyle/assets/js/popper.min.js')}}"></script>
        <script src="{{ URL::asset('adminstyle/assets/js/bootstrap.min.js')}}"></script>
        <script src="{{ URL::asset('adminstyle/assets/js/waves.js')}}"></script>
        <script src="{{ URL::asset('adminstyle/assets/js/jquery.slimscroll.js')}}"></script>
        <script src="{{ URL::asset('adminstyle/plugins/jquery-toastr/jquery.toast.min.js')}}"></script>

        <!-- App js -->
        <script src="{{ URL::asset('adminstyle/assets/js/jquery.core.js')}}"></script>
        <script src="{{ URL::asset('adminstyle/assets/js/jquery.app.js')}}"></script>
        <script>
$(document).ready(function() {
@if (session('status'))
@php
$status = json_decode(session('status'), true);
@endphp
$.toast({heading:"{{ $status['status']}}!",text:"{{ $status['message']}}",
position:"top-right",loaderBg:"#3b98b5",icon:"{{ strtolower($status['status'])}}",hideAfter:3e3,stack:1})
@endif
});
        </script>
    </body>
</html>
