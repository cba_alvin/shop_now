<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'id'       => 1,
            'status'   => 2,
            'name'     => 'admin',
            'email'    => 'admin@gmail.com',
            'password' => bcrypt('admin'),
        ]);
        DB::table('category_products')->insert([
            'name'             => 'All',
            'parent_id'        => 0,
            'pin_index_status' => 1,
            'status'           => 1,
        ]);

        DB::table('abouts')->insert([
            'title'              => 'Modern stylish home interiors',
            'json_image_details' => '{"1":{"name":"frontend\/img\/blog\/single01.jpg"},"2":{"name":"frontend\/img\/blog\/single02.jpg"},"3":{"name":"frontend\/img\/blog\/single03.jpg"}}',
            'discription'        => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Ut enim ad minima veniam, quis nostrum exercitationem.</p>

<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

<blockquote>
<p>Perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo.</p>
<cite>Someone famous</cite></blockquote>

<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga.</p>',
        ]);
    }
}
