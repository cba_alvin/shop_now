<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_product_id')->nullable();
            $table->string('name')->nullable();
            $table->string('name_slug')->nullable();
            $table->bigInteger('price')->nullable()->default(0);
            $table->bigInteger('price_sale')->nullable()->default(0);
            $table->integer('sell_through')->nullable()->default(0);
            $table->integer('status')->nullable()->default(0);
            $table->text('image_index')->nullable();
            $table->longText('json_image_details')->nullable();
            $table->longText('discription')->nullable();
            $table->longText('json_details')->nullable();
            $table->timestamp('public_at')->nullable();
            $table->timestamp('unpublic_at')->nullable();
            $table->bigInteger('views')->nullable()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
