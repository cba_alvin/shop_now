<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
Route::get('/test', function () {
    return view('admin.dashboard_test');
});

Route::get('/', 'User\ProductController@index')->name('home');
Route::get('product/{slug?}', 'User\ProductController@singleProduct')->name('product.single');
Route::get('product/search/{type?}', 'User\ProductController@searchProduct')->name('product.search');
Route::get('category/{id?}', 'User\ProductController@category')->name('product.category');

//Other

Route::get('about', 'User\OtherController@about')->name('other.about');
Route::get('contact', 'User\OtherController@contact')->name('other.contact');
Route::post('contact', 'User\OtherController@postContact')->name('other.postcontact');

// Admin

Route::get('admin/login', 'Auth\Admin\AuthController@getLogin')->name('admin.getlogin');
Route::post('admin/login', 'Auth\Admin\AuthController@postLogin')->name('admin.postlogin');
Route::get('admin/logout', 'Auth\Admin\AuthController@getLogout')->name('admin.getLogout');
Route::group(['prefix' => 'admin', 'middleware' => ['adminlogin']], function () {

    Route::get('/', 'Admin\DashboardController@getIndex')->name('admin.dashboard');

    Route::get('about', 'Admin\AboutController@getStore')->name('admin.about.store');
    Route::post('about', 'Admin\AboutController@postStore')->name('admin.about.store');

    Route::group(['prefix' => 'uploads'], function () {
        Route::post('image/{type?}', 'Admin\UploadAjaxController@uploadImageProduct')->name('admin.uploads.image');
    });

    Route::group(['prefix' => 'contact'], function () {
        Route::get('/', 'Admin\ContactController@index')->name('admin.contact.list');
        Route::get('detail/{id}', 'Admin\ContactController@detail')->name('admin.contact.detail');
        Route::post('delete/{id?}', 'Admin\ContactController@postDelete')->name('admin.contact.delete');
    });

    Route::group(['prefix' => 'banner', 'middleware' => ['role:admin']], function () {
        Route::get('/', 'Admin\BannerController@index')->name('admin.banner.list')->middleware('permission:update');
        Route::get('store/{id?}', 'Admin\BannerController@getStore')->name('admin.banner.store');
        Route::post('store/{id?}', 'Admin\BannerController@postStore')->name('admin.banner.store');
        Route::post('delete', 'Admin\BannerController@postDelete')->name('admin.banner.delete');
    });

    Route::group(['prefix' => 'user'], function () {
        Route::get('/', 'Admin\UserController@index')->name('admin.user.list');
        Route::get('store/{id?}', 'Admin\UserController@getStore')->name('admin.user.store');
        Route::post('store/{id?}', 'Admin\UserController@postStore')->name('admin.user.store');
        Route::post('delete', 'Admin\UserController@postDelete')->name('admin.user.delete');
    });

    Route::group(['prefix' => 'permission'], function () {
        Route::get('/', 'Admin\PermissionController@index')->name('admin.permission.list');
        Route::get('store/{id?}', 'Admin\PermissionController@getStore')->name('admin.permission.store');
        Route::post('store/{id?}', 'Admin\PermissionController@postStore')->name('admin.permission.store');
        Route::post('delete', 'Admin\PermissionController@postDelete')->name('admin.permission.delete');
    });

    Route::group(['prefix' => 'role'], function () {
        Route::get('/', 'Admin\RoleController@index')->name('admin.role.list');
        Route::get('store/{id?}', 'Admin\RoleController@getStore')->name('admin.role.store');
        Route::post('store/{id?}', 'Admin\RoleController@postStore')->name('admin.role.store');
        Route::post('delete', 'Admin\RoleController@postDelete')->name('admin.role.delete');
    });
    Route::group(['prefix' => 'products'], function () {

        Route::group(['prefix' => 'category'], function () {
            Route::get('/', 'Admin\CategoryProductController@index')->name('admin.products.category.list');
            Route::get('store/{id?}', 'Admin\CategoryProductController@getStore')->name('admin.products.category.store');
            Route::post('store/{id?}', 'Admin\CategoryProductController@postStore')->name('admin.products.category.store');
            Route::post('delete', 'Admin\CategoryProductController@postDelete')->name('admin.products.category.delete');
        });

        Route::group(['prefix' => 'product'], function () {
            Route::get('/', 'Admin\ProductController@index')->name('admin.products.product.list');
            Route::get('store/{id?}', 'Admin\ProductController@getStore')->name('admin.products.product.store');
            Route::post('store/{id?}', 'Admin\ProductController@postStore')->name('admin.products.product.store');
            Route::post('delete', 'Admin\ProductController@postDelete')->name('admin.products.product.delete');
        });

    });

});
